+++
title = "協力メンバー"
linkTitle = ""
subtitle = "Contributors"
date = "2022-06-16"
categories = []
tags = []
weight = 100
draft = false
share_img = ""
[[bigimg]]
src = "/kids/img/jacst_for_kids.jpg"
desc = "2022-06-16 現在"

+++


- 倉田智子（基生研）
- 入江敦子（KEK）
- 髙橋将太（KEK）
- 高橋涼香（理研BDR）
- 高宮ミンディ（京大アイセムス）
- 武長祐美子（東大宇宙線研神岡）
- 中道康文（NIMS）
- 西村有香（QST）
- 皆川麻利江（東大齊藤研）
- 山口三菜子（セラ協）
- 三好明子（熊本大学）
- 山岡均（国立天文台）
