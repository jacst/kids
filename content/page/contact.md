+++
title = "お問い合わせ"
subtitle = "Contact"
date = "2020-02-29"
weight = "3"
[[bigimg]]
src =  "/kids/img/jacst_for_kids.jpg"
desc = "お問い合わせ"
+++

取材依頼、ご感想、ご要望などがございましたら、下記「このサイトの利用について」の部分をご確認のうえ、こちらの[お問い合わせフォーム（Googleフォーム）](https://forms.gle/xDeop2udd7ZaYCHN8)にご記入をお願いします。


---

## 科学技術広報研究会（JACST）について

科学技術広報研究会（JACST：Japan Association of Communication for Science and Technology ）は、研究機関や大学などの広報担当者が、所属する組織の枠をこえて、広報活動における課題を共有し、それらを通してお互いに助け合い、共に成長していくことを目指したネットワークです。約80機関から約140名の広報担当者が参加しています（2020年3月2日現在）

- ウェブサイト：[https://sites.google.com/view/jacst/](https://sites.google.com/view/jacst/)
- 問い合わせ先：JACST事務局（jacst-office@googlegroups.com）
