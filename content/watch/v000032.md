+++
title = "望遠鏡ペーパークラフト"
linkTitle = ""
subtitle = ""
date = "2020-02-29"
categories = [
    "工作系",
]
tags = [
    "ウェブサイト",
    "ニュース利用可能",
    "国立天文台",
    "小学生",
    "工作",
    "日本語",
]
weight = 0
draft = false
share_img = ""
bigimg = []
hosts = [
    "国立天文台",
]
services = [
    "ウェブサイト",
]
ages = [
    "小学生",
]
subject = []
author = "国立天文台"
+++


<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-02-29

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.nao.ac.jp/gallery/paper-craft/telescope.html
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 自然科学研究機構　国立天文台（国立天文台）
- <i class="fas fa-globe-asia"></i> &nbsp; 東京都三鷹市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.nao.ac.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

国立天文台


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


