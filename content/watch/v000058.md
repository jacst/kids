+++
title = "幹細胞ハンドブック"
linkTitle = ""
subtitle = ""
date = "2020-02-29"
categories = [
    "よみもの系",
]
tags = [
    "よみもの",
    "ウェブサイト",
    "ニュース利用可能",
    "京都大学iPS細胞研究所",
    "日本語",
]
weight = 0
draft = false
share_img = ""
bigimg = []
hosts = [
    "京都大学iPS細胞研究所",
]
services = [
    "ウェブサイト",
]
ages = [
    "未分類",
]
subject = []
author = "京都大学CiRA"
+++


<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-02-29

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.cira.kyoto-u.ac.jp/j/pressrelease/pdf/stemcellhandbook_revised10_141215.pdf?1582951892083
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 京都大学　iPS細胞研究所（京都大学CiRA）
- <i class="fas fa-globe-asia"></i> &nbsp; 京都府京都市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.cira.kyoto-u.ac.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

京都大学iPS細胞研究所


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


