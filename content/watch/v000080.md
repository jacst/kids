+++
title = "その２・気候 [変] 会議－温暖化時代をきみはどう生きる？－"
linkTitle = ""
subtitle = ""
date = "2020-03-02"
categories = [
    "講演会など",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "動画",
    "国立環境研究所",
    "日本語",
    "高校生",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/Z-b-IBYC7EI/hqdefault.jpg"
bigimg = []
hosts = [
    "国立環境研究所",
]
services = [
    "YouTube",
]
ages = [
    "高校生",
]
subject = []
author = "国環研"
+++
{{< youtube id="Z-b-IBYC7EI" title="その２・気候 [変] 会議－温暖化時代をきみはどう生きる？－" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-03-02

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://youtu.be/Z-b-IBYC7EI
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 国立環境研究所（国環研）
- <i class="fas fa-globe-asia"></i> &nbsp; 茨城県つくば市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.nies.go.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

国立環境研究所


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


