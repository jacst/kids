+++
title = "サイエンス異種格闘技戦(2019/1/17)坪田 誠学部長"
linkTitle = ""
subtitle = ""
date = "2020-03-02"
categories = [
    "講演会など",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "大阪市立大学",
    "日本語",
    "講演会",
    "高校生",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/z0BKxHMsvmw/hqdefault.jpg"
bigimg = []
hosts = [
    "大阪市立大学",
]
services = [
    "YouTube",
]
ages = [
    "高校生",
]
subject = []
author = "大阪市立大学"
+++
{{< youtube id="z0BKxHMsvmw" title="サイエンス異種格闘技戦(2019/1/17)坪田 誠学部長" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-03-02

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.youtube.com/watch?v=z0BKxHMsvmw
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 大阪市立大学（大阪市立大学）
- <i class="fas fa-globe-asia"></i> &nbsp; 大阪府大阪市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.osaka-cu.ac.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

大阪市立大学


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


