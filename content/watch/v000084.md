+++
title = "イグ・ノーベル賞が北大にやってきた！【研究室訪問編】"
linkTitle = ""
subtitle = ""
date = "2020-03-02"
categories = [
    "講演会など",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "動画",
    "北海道大学",
    "日本語",
    "高校生",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/L-VhsNF6Qt8/hqdefault.jpg"
bigimg = []
hosts = [
    "北海道大学",
]
services = [
    "YouTube",
]
ages = [
    "高校生",
]
subject = []
author = "北海道大学"
+++
{{< youtube id="L-VhsNF6Qt8" title="イグ・ノーベル賞が北大にやってきた！【研究室訪問編】" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-03-02

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.youtube.com/watch?v=L-VhsNF6Qt8&t=23s
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 北海道大学（北海道大学）
- <i class="fas fa-globe-asia"></i> &nbsp; 北海道札幌市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.hokudai.ac.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

北海道大学


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


