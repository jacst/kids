+++
title = "《第2日目》KEK春のキャンパス公開2022・オンライン"
linkTitle = ""
subtitle = "《だい2にちめ》KEKはるのきゃんぱすこうかい2022・おんらいん"
date = "2022-04-23"
categories = [
    "一般公開",
]
tags = [
    "YouTube",
    "ライブ配信",
    "日本語",
    "物理",
    "誰でも",
    "高エネルギー加速器研究機構",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/POV1rikg284/hqdefault.jpg"
bigimg = []
hosts = [
    "高エネルギー加速器研究機構",
]
services = [
    "YouTube",
]
ages = [
    "誰でも",
]
subject = [
    "物理",
]
author = "KEK"
+++
{{< youtube id="POV1rikg284" title="《第2日目》KEK春のキャンパス公開2022・オンライン" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2022-04-23

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://youtu.be/POV1rikg284
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 高エネルギー加速器研究機構（KEK）
- <i class="fas fa-globe-asia"></i> &nbsp; 茨城県つくば市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.kek.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

KEK


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から許諾を得ていません。
ご不明な点に関しては、提供機関に直接お問い合わせください。


