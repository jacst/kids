+++
title = "研究者インタビュー　みんな違ってみんないい　～谷口真人教授～"
linkTitle = ""
subtitle = ""
date = "2020-03-06"
categories = [
    "講演会など",
]
tags = [
    "ウェブサイト",
    "ニュース利用可能",
    "中学生",
    "動画",
    "日本語",
    "総合地球環境学研究所",
]
weight = 0
draft = false
share_img = ""
bigimg = []
hosts = [
    "総合地球環境学研究所",
]
services = [
    "ウェブサイト",
]
ages = [
    "中学生",
]
subject = []
author = "地球研"
+++


<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-03-06

## <i class="fas fa-paperclip"></i> &nbsp; URL

- http://chikyuken.sakura.ne.jp/members/002?branch=1
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 人間文化研究機構　総合地球環境学研究所（地球研）
- <i class="fas fa-globe-asia"></i> &nbsp; 京都府京都市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.chikyu.ac.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

総合地球環境学研究所


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


