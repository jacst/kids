+++
title = "半世紀の研究を振り返って -基礎科学の将来- 大隈良典"
linkTitle = ""
subtitle = ""
date = "2020-03-02"
categories = [
    "講演会など",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "中学生",
    "国立遺伝学研究所",
    "日本語",
    "講演会",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/gpBW3-4P1so/hqdefault.jpg"
bigimg = []
hosts = [
    "国立遺伝学研究所",
]
services = [
    "YouTube",
]
ages = [
    "中学生",
]
subject = []
author = "NIG"
+++
{{< youtube id="gpBW3-4P1so" title="半世紀の研究を振り返って -基礎科学の将来- 大隈良典" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-03-02

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.youtube.com/watch?v=gpBW3-4P1so
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 情報・システム研究機構　国立遺伝学研究所（NIG）
- <i class="fas fa-globe-asia"></i> &nbsp; 静岡県三島市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.nig.ac.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

国立遺伝学研究所


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


