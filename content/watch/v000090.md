+++
title = "裁判劇とワークショップ「私の仕事を決めるのは誰？～裁判劇を通じて人工知能を用いた人事評価の是非を考える」"
linkTitle = ""
subtitle = ""
date = "2020-03-02"
categories = [
    "のんびり系",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "動画",
    "北海道大学CoSTEP",
    "日本語",
    "高校生",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/_w6ZwTN4iqY/hqdefault.jpg"
bigimg = []
hosts = [
    "北海道大学CoSTEP",
]
services = [
    "YouTube",
]
ages = [
    "高校生",
]
subject = []
author = "北海道大学CoSTEP"
+++
{{< youtube id="_w6ZwTN4iqY" title="裁判劇とワークショップ「私の仕事を決めるのは誰？～裁判劇を通じて人工知能を用いた人事評価の是非を考える」" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-03-02

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.youtube.com/watch?v=_w6ZwTN4iqY
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 北海道大学　CoSTEP（北海道大学CoSTEP）
- <i class="fas fa-globe-asia"></i> &nbsp; 北海道札幌市
- <i class="fas fa-laptop-code"></i> &nbsp; https://costep.open-ed.hokudai.ac.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

北海道大学 CoSTEP


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


