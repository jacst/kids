+++
title = "【世界初へ挑戦】深海5,000メートルへの有人科学探査を生中継～JAMSTEC×ニコ生"
linkTitle = ""
subtitle = ""
date = "2020-02-28"
categories = [
    "のんびり系",
]
tags = [
    "ニコニコ",
    "ニュース利用可能",
    "中学生",
    "動画",
    "日本語",
    "海洋研究開発機構",
]
weight = 0
draft = false
share_img = ""
bigimg = []
hosts = [
    "海洋研究開発機構",
]
services = [
    "ニコニコ",
]
ages = [
    "中学生",
]
subject = []
author = "JAMSTEC"
+++
{{< niconico id="lv139636921" title="【世界初へ挑戦】深海5,000メートルへの有人科学探査を生中継～JAMSTEC×ニコ生" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-02-28

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://live.nicovideo.jp/watch/lv139636921
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 海洋研究開発機構（JAMSTEC）
- <i class="fas fa-globe-asia"></i> &nbsp; 神奈川県横須賀市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.jamstec.go.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

JAMSTEC/niconico


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


