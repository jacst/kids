+++
title = "最新研究映像　NIMSの力！「磁石と蛍光ペンで、壊さずに内部をのぞき込む！　～非破壊検査のヒミツ！～」"
linkTitle = ""
subtitle = ""
date = "2020-02-29"
categories = [
    "さっと系",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "動画",
    "日本語",
    "物質・材料研究機構",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/zVU_JC_I1h0/hqdefault.jpg"
bigimg = []
hosts = [
    "物質・材料研究機構",
]
services = [
    "YouTube",
]
ages = [
    "未分類",
]
subject = []
author = "NIMS"
+++
{{< youtube id="zVU_JC_I1h0" title="最新研究映像　NIMSの力！「磁石と蛍光ペンで、壊さずに内部をのぞき込む！　～非破壊検査のヒミツ！～」" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-02-29

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.youtube.com/watch?v=zVU_JC_I1h0
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 物質・材料研究機構（NIMS）
- <i class="fas fa-globe-asia"></i> &nbsp; 茨城県つくば市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.nims.go.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

物質・材料研究機構


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


