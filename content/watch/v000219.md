+++
title = "15分で脳科学「愛と憎しみの脳科学 －人間の親密性と攻撃性を神経科学から考える－」"
linkTitle = ""
subtitle = ""
date = "2020-03-09"
categories = [
    "講演会など",
]
tags = [
    "YouTube",
    "日本語",
    "理化学研究所脳神経科学研究センター",
    "講演会",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/5zUJ8OQobqM/hqdefault.jpg"
bigimg = []
hosts = [
    "理化学研究所脳神経科学研究センター",
]
services = [
    "YouTube",
]
ages = [
    "未分類",
]
subject = []
author = "RIKEN CBS"
+++
{{< youtube id="5zUJ8OQobqM" title="15分で脳科学「愛と憎しみの脳科学 －人間の親密性と攻撃性を神経科学から考える－」" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-03-09

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.youtube.com/watch?v=5zUJ8OQobqM
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 理化学研究所　脳神経科学研究センター（RIKEN CBS）
- <i class="fas fa-globe-asia"></i> &nbsp; 埼玉県和光市
- <i class="fas fa-laptop-code"></i> &nbsp; https://cbs.riken.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

未分類


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から許諾を得ていません。
ご不明な点に関しては、提供機関に直接お問い合わせください。


