+++
title = "【第7話】どうして夜は暗いの？"
linkTitle = ""
subtitle = "どうしてよるはくらいの？"
date = "2022-02-04"
categories = [
    "さっと系",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "動画",
    "小学生",
    "日本語",
    "高エネルギー加速器研究機構",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/Uf5kafJumMg/hqdefault.jpg"
bigimg = []
hosts = [
    "高エネルギー加速器研究機構",
]
services = [
    "YouTube",
]
ages = [
    "小学生",
]
subject = []
author = "KEK"
+++
{{< youtube id="Uf5kafJumMg" title="【第7話】どうして夜は暗いの？" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2022-02-04

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://youtu.be/Uf5kafJumMg
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 高エネルギー加速器研究機構（KEK）
- <i class="fas fa-globe-asia"></i> &nbsp; 茨城県つくば市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.kek.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

KEK


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


