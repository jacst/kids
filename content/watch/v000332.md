+++
title = "【ゆっくり解説】重粒子線がん治療装置HIMAC -加速器編-"
linkTitle = ""
subtitle = "ゆっくりかいせつ　じゅうりゅうしせんがんちりょうそうち　はいまっく　かそくきへん"
date = "2021-04-26"
categories = [
    "のんびり系",
]
tags = [
    "YouTube",
    "ニュース利用不可",
    "動画",
    "日本語",
    "物理",
    "理科",
    "誰でも",
    "量子科学技術研究開発機構",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/0cYDRaEfmE8/hqdefault.jpg"
bigimg = []
hosts = [
    "量子科学技術研究開発機構",
]
services = [
    "YouTube",
]
ages = [
    "誰でも",
]
subject = [
    "物理",
    "理科",
]
author = "QST"
+++
{{< youtube id="0cYDRaEfmE8" title="【ゆっくり解説】重粒子線がん治療装置HIMAC -加速器編-" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2021-04-26

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://youtu.be/0cYDRaEfmE8
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 量子科学技術研究開発機構（QST）
- <i class="fas fa-globe-asia"></i> &nbsp; 千葉県千葉市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.qst.go.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

QST


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から許諾を得ていません。
ご不明な点に関しては、提供機関に直接お問い合わせください。


