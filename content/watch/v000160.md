+++
title = "日本未来ばなし「インフルエンザと谷風」"
linkTitle = ""
subtitle = ""
date = "2020-03-05"
categories = [
    "アニメ系",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "動画",
    "日本科学未来館",
    "日本語",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/okK2gciH-ks/hqdefault.jpg"
bigimg = []
hosts = [
    "日本科学未来館",
]
services = [
    "YouTube",
]
ages = [
    "未分類",
]
subject = []
author = "未来館"
+++
{{< youtube id="okK2gciH-ks" title="日本未来ばなし「インフルエンザと谷風」" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-03-05

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.youtube.com/watch?v=okK2gciH-ks
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 日本科学未来館（未来館）
- <i class="fas fa-globe-asia"></i> &nbsp; 東京都江東区
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.miraikan.jst.go.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

日本科学未来館


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


