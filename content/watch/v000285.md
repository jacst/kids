+++
title = "シミュレーションってどんなことするの？"
linkTitle = ""
subtitle = ""
date = "2020-05-08"
categories = [
    "さっと系",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "動画",
    "小学生",
    "日本語",
    "理化学研究所計算科学研究センター",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/mA6gG15Mcew/hqdefault.jpg"
bigimg = []
hosts = [
    "理化学研究所計算科学研究センター",
]
services = [
    "YouTube",
]
ages = [
    "小学生",
]
subject = []
author = "RIKEN R-CCS"
+++
{{< youtube id="mA6gG15Mcew" title="シミュレーションってどんなことするの？" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-05-08

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://youtu.be/mA6gG15Mcew
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 理化学研究所　計算科学研究センター（RIKEN R-CCS）
- <i class="fas fa-globe-asia"></i> &nbsp; 兵庫県神戸市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.r-ccs.riken.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

理化学研究所


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


