+++
title = "良い音と悪い音の科学 －お寺のお経がありがたく、カラスの鳴き声が不快なのはなぜか－"
linkTitle = ""
subtitle = ""
date = "2020-02-29"
categories = [
    "講演会など",
]
tags = [
    "YouTube",
    "ニュース利用申請",
    "動画",
    "日本語",
    "産業技術総合研究所",
    "高校生",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/Epph9iUobrQ/hqdefault.jpg"
bigimg = []
hosts = [
    "産業技術総合研究所",
]
services = [
    "YouTube",
]
ages = [
    "高校生",
]
subject = []
author = "産総研"
+++
{{< youtube id="Epph9iUobrQ" title="良い音と悪い音の科学 －お寺のお経がありがたく、カラスの鳴き声が不快なのはなぜか－" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-02-29

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.youtube.com/watch?v=Epph9iUobrQ&t=16s
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 産業技術総合研究所（産総研）
- <i class="fas fa-globe-asia"></i> &nbsp; 茨城県つくば市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.aist.go.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

産総研 or 産業技術総合研究所


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から申請が必要だと聞いています。
提供機関に直接お問い合わせください。


