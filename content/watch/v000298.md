+++
title = "JAMSTEC海の研究こども質問部屋（第3回 海の生き物編（その１））"
linkTitle = ""
subtitle = ""
date = "2020-06-10"
categories = [
    "休校中特別授業",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "ライブ配信",
    "小学生",
    "日本語",
    "海洋研究開発機構",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/GkecroxvTRg/hqdefault.jpg"
bigimg = []
hosts = [
    "海洋研究開発機構",
]
services = [
    "YouTube",
]
ages = [
    "小学生",
]
subject = []
author = "JAMSTEC"
+++
{{< youtube id="GkecroxvTRg" title="JAMSTEC海の研究こども質問部屋（第3回 海の生き物編（その１））" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-06-10

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://youtu.be/GkecroxvTRg?list=PL97pirzgh57OZNTwV2Dk0QFUkZSSnr90I
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 海洋研究開発機構（JAMSTEC）
- <i class="fas fa-globe-asia"></i> &nbsp; 神奈川県横須賀市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.jamstec.go.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

JAMSTEC


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


