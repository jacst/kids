+++
title = "大型低温重力波望遠鏡KAGRA"
linkTitle = ""
subtitle = ""
date = "2020-02-29"
categories = [
    "さっと系",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "動画",
    "日本語",
    "東京大学宇宙線研究所",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/iJa_Ym1n8CQ/hqdefault.jpg"
bigimg = []
hosts = [
    "東京大学宇宙線研究所",
]
services = [
    "YouTube",
]
ages = [
    "未分類",
]
subject = []
author = "ICRR"
+++
{{< youtube id="iJa_Ym1n8CQ" title="大型低温重力波望遠鏡KAGRA" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-02-29

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://youtu.be/iJa_Ym1n8CQ
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 東京大学　宇宙線研究所（ICRR）
- <i class="fas fa-globe-asia"></i> &nbsp; 千葉県柏市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.icrr.u-tokyo.ac.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

東京大学宇宙線研究所


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


