+++
title = "イベントディスプレイを作ってみよう"
linkTitle = ""
subtitle = ""
date = "2020-06-01"
categories = [
    "ゲーム系",
]
tags = [
    "その他",
    "ウェブサイト",
    "ニュース利用可能",
    "日本語",
    "東京大学宇宙線研究所神岡宇宙素粒子研究施設",
    "誰でも",
]
weight = 0
draft = false
share_img = ""
bigimg = []
hosts = [
    "東京大学宇宙線研究所神岡宇宙素粒子研究施設",
]
services = [
    "ウェブサイト",
]
ages = [
    "誰でも",
]
subject = []
author = "ICRR（SK）"
+++


<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-06-01

## <i class="fas fa-paperclip"></i> &nbsp; URL

- http://www-sk.icrr.u-tokyo.ac.jp/sk/detector/eventdisplay-data.html
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 東京大学　宇宙線研究所　神岡宇宙素粒子研究施設（ICRR（SK））
- <i class="fas fa-globe-asia"></i> &nbsp; 岐阜県飛騨市
- <i class="fas fa-laptop-code"></i> &nbsp; http://www-sk.icrr.u-tokyo.ac.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

東京大学宇宙線研究所神岡宇宙素粒子研究施設


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


