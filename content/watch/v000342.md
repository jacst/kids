+++
title = "乗っ取り再生は可能か？プラナリア(ナミウズムシ)の幹細胞をキタシロウズムシに移植～中間報告編～【ニコニコサイエンス】"
linkTitle = ""
subtitle = ""
date = "2022-01-28"
categories = [
    "のんびり系",
]
tags = [
    "ニコニコ",
    "動画",
    "基礎生物学研究所",
    "日本語",
    "誰でも",
]
weight = 0
draft = false
share_img = ""
bigimg = []
hosts = [
    "基礎生物学研究所",
]
services = [
    "ニコニコ",
]
ages = [
    "誰でも",
]
subject = []
author = "基生研"
+++
{{< niconico id="lv335350703" title="乗っ取り再生は可能か？プラナリア(ナミウズムシ)の幹細胞をキタシロウズムシに移植～中間報告編～【ニコニコサイエンス】" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2022-01-28

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://live.nicovideo.jp/watch/lv335350703
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 自然科学研究機構　基礎生物学研究所（基生研）
- <i class="fas fa-globe-asia"></i> &nbsp; 愛知県岡崎市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.nibb.ac.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

未分類


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から許諾を得ていません。
ご不明な点に関しては、提供機関に直接お問い合わせください。


