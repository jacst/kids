+++
title = "ものしり新聞"
linkTitle = ""
subtitle = ""
date = "2020-03-04"
categories = [
    "よみもの系",
]
tags = [
    "よみもの",
    "ウェブサイト",
    "ニュース利用可能",
    "中学生",
    "日本語",
    "東京大学カブリ数物連携宇宙研究機構",
]
weight = 0
draft = false
share_img = ""
bigimg = []
hosts = [
    "東京大学カブリ数物連携宇宙研究機構",
]
services = [
    "ウェブサイト",
]
ages = [
    "中学生",
]
subject = []
author = "Kavli IPMU"
+++


<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-03-04

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.ipmu.jp/ja/public-communications/MonoshiriShimbun
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 東京大学　カブリ数物連携宇宙研究機構（Kavli IPMU）
- <i class="fas fa-globe-asia"></i> &nbsp; 千葉県柏市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.ipmu.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

Kavli IPMU


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


