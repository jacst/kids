+++
title = "ロボットやICTを活用したスマート農業の実現に向けて"
linkTitle = ""
subtitle = ""
date = "2020-03-04"
categories = [
    "講演会など",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "動画",
    "日本語",
    "農業・食品産業技術総合研究機構",
]
weight = 0
draft = false
share_img = "https://img.youtube.com/vi/sueAqarmwz4/hqdefault.jpg"
bigimg = []
hosts = [
    "農業・食品産業技術総合研究機構",
]
services = [
    "YouTube",
]
ages = [
    "未分類",
]
subject = []
author = "農研機構"
+++
{{< youtube id="sueAqarmwz4" title="ロボットやICTを活用したスマート農業の実現に向けて" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-03-04

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.youtube.com/watch?v=sueAqarmwz4
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 農業・食品産業技術総合研究機構（農研機構）
- <i class="fas fa-globe-asia"></i> &nbsp; 茨城県つくば市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.naro.go.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

農研機構


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


