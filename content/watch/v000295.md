+++
title = "ウェブ de NIMS一般公開 2020"
linkTitle = ""
subtitle = ""
date = "2020-05-29"
categories = [
    "一般公開",
]
tags = [
    "YouTube",
    "ニュース利用可能",
    "ライブ配信",
    "日本語",
    "物質・材料研究機構",
    "誰でも",
]
weight = 0
draft = false
share_img = ""
bigimg = []
hosts = [
    "物質・材料研究機構",
]
services = [
    "YouTube",
]
ages = [
    "誰でも",
]
subject = []
author = "NIMS"
+++
{{< youtube id="未分類" title="ウェブ de NIMS一般公開 2020" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-05-29

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.nims.go.jp/openhouse/
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 物質・材料研究機構（NIMS）
- <i class="fas fa-globe-asia"></i> &nbsp; 茨城県つくば市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.nims.go.jp


---

## <i class="fas fa-copyright"></i> &nbsp; クレジット

NIMS


---

## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について

このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。
ご利用の際はクレジットの明記をお願いします。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。


