+++
title = "全地球史アトラス / The Whole History of the Earth and Life"
linkTitle = ""
subtitle = ""
date = "2020-02-29"
categories = [ "さっと系",]
tags = [ "YouTube", "ニュース利用可能", "動画", "情報・システム研究機構\u3000国立遺伝学研究所", "日本語", "高校生",]
weight = 0
draft = false
share_img = ""
bigimg = []
hosts = [ "国立遺伝学研究所",]
services = [ "YouTube",]
ages = [ "高校生",]
subject = []
author = "NIG"
+++
{{< youtube-playlist id="PLmHR2mJfCqKLuxkaZDVUXl_fuA4REC5L4" title="全地球史アトラス / The Whole History of the Earth and Life" >}}



<!--more-->

## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日

2020-02-29

## <i class="fas fa-paperclip"></i> &nbsp; URL

- https://www.youtube.com/playlist?list=PLmHR2mJfCqKLuxkaZDVUXl_fuA4REC5L4
- 埋め込みが正しく表示されない場合は上記URLをクリックしてください

---

## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関

- <i class="fas fa-landmark"></i> &nbsp; 情報・システム研究機構　国立遺伝学研究所（NIG）
- <i class="fas fa-globe-asia"></i> &nbsp; 静岡県三島市
- <i class="fas fa-laptop-code"></i> &nbsp; https://www.nig.ac.jp


---

## <i class="fas fa-copy"></i> &nbsp; ニュース利用時のクレジットについて

このコンテンツは、提供機関の広報担当者から、テレビなどのニュースの一部として利用可能という承諾を得てあります。
ご利用の際はクレジットの明記をお願いします。
クレジットは「文科省 科研費新学術領域 冥王代生命学」をお使いください。
その他、ご不明な点に関しては、提供機関に直接お問い合わせください。
