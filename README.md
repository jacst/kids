# 科学技術広報研究会（JACST）臨時休校対応特別企画

休校中の子供たちにぜひ見て欲しい科学技術の面白デジタルコンテンツ
--- 各研究機関の広報担当者がセレクトしました ---

---

# 🤖 ウェブサイトを移行しました（2022/03）

[オリジナルのGoogleサイト](https://sites.google.com/view/jacst-for-kids)から、
[こちらのGitLab Pages](https://jacst.gitlab.io/kids/) へと移行しました。
今後、スプレッドシートに追加されたコンテンツはGitLab Pagesにのみ掲載します。

## サイト移行の目的

オリジナルサイトでは、コンテンツの掲載を手入力していました。
掲載作業になかなか手間がかかることもあり、
サイトの需要のピークが過ぎたあと、コンテンツの更新は（ほぼ）停滞してしまっていました。


掲載作業の自動化は、オリジナルサイトを開設した直後からの課題だったのですが、
約2年かかってようやく、自動で更新できる仕組みを整えることができました。


これからは、簡単なスプレッドシートの管理のみで、ウェブサイトの更新ができるようになります。

#  📺 コンテンツの追加について


この1〜2年の間に動画コンテンツを強化した大学・研究機関も多いと思います。
その中で「子供たちに見てほしい」おすすめのコンテンツがありましたら、
引き続き提供していただけますと幸いです。

ただし、コンテンツを追加できるのは原則JACSTの会員のみとします。
具体的な追加方法に関しては会員専用MLでお知らせします。


# ⚙️ ウェブサイトの更新フロー

![](./workflow.drawio.svg)

# ウェブサイトの修正報告など

ウェブサイトの不具合などを発見した場合は ``issues報告``機能を使ってお知らせください。

- [Issues 機能](https://gitlab.com/jacst/kids/-/issues)

具体的な手順については [CONTRIBUTING.md](CONTRIBUTING.md) をご確認ください。

---
# 開発者向け

## 一括して実行

```bash
# プロジェクトルートに移動する
$ cd ~/repos/gitlab/jacst-kids

# スクリプト用のディレクトリに移動
$ cd scripts

# スクリプトを実行
$ poetry run run.sh
```

## 定期実行

- ``macOS``の``Launchd``を使って定期実行できるようにした

```bash
# プロジェクトルートに移動する
$ cd ~/repos/gitlab/jacst-kids

# lanchctrl関係をまとめたディレクトリに移動
$ cd launchd

# Launchd に登録 /削除
$ make install
$ make uninstall
```

## ``run.sh``の実行内容


- ``run.sh``はPythonスクリプトをまとめただけのシェルスクリプト
- Pythonスクリプトはそれぞれ単独で実行することができる

```bash
# プロジェクトルートに移動する
$ cd ~/repos/gitlab/jacst-kids

# poetryを使って仮想環境を立ち上げる
$ poetry shell

# スクリプト用のディレクトリに移動する
(poetry-venv) $ cd scripts

# GoogleシートをCSV形式でダウンロード
(poetry-venv) $ snapsheets

📣 JACST臨時休校特別対応サイト・コンテンツの一覧
🤖 Downloaded snapd/snapsheet.xlsx
🚀 Renamed to snapd/_jk_contents.csv

📣 JACST臨時休校特別対応サイト・協力メンバーの一覧
🤖 Downloaded snapd/snapsheet.xlsx
🚀 Renamed to snapd/_jk_contributors.csv

📣 JACST臨時休校特別対応サイト・参加機関の一覧
🤖 Downloaded snapd/snapsheet.xlsx
🚀 Renamed to snapd/_jk_hosts.csv

📣 JACST臨時休校特別対応サイト・活動報告の一覧
🤖 Downloaded snapd/snapsheet.xlsx
🚀 Renamed to snapd/_jk_reports.csv

# コンテンツ生成に必要なファイルを作成する
(poetry-venv) $ ./merger.py
Merged dataframes ("snapd/_jk_contents.csv" / "snapd/_jk_hosts.csv")
Saved as contents.csv
Saved as reports.csv

# Hugoのコンテンツ用ファイルを /content/watch/v連番.md に作成する
(poetry-venv) $ ./parse.py
Created 318 pages
Created 10 recommended pages
Updated ../content/_index.md
```
