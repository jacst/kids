from dataclasses import field

import pandas as pd
from loguru import logger
from serde import serde
from serde.toml import to_toml
from serde.yaml import to_yaml


@serde
class FMBase:
    title: str = ""
    linkTitle: str = ""
    subtitle: str = ""
    date: str = ""
    categories: list[str] = field(default_factory=list)
    tags: list[str] = field(default_factory=list)
    weight: int = 0
    draft: bool = True
    share_img: str = ""
    bigimg: list = field(default_factory=list)

    def as_toml(self):
        """TOML formatted frontmatter"""
        lines = "+++\n"
        lines += to_toml(self)
        lines += "+++\n"
        return lines

    def as_yaml(self):
        """YAML formatted frontmatter"""
        lines = "---\n"
        lines += to_yaml(self)
        lines += "---\n"
        return lines

    def as_debug(self):
        """debug"""
        lines = "```toml\n"
        lines += self.as_toml()
        lines += "```\n"
        return lines

    def add_default_values(self, row):
        """
        pandasのデータフレームの行の中身をフロントマターに代入する。
        その際に、いろいろと前処理を加える

        Parameters
        ----------
        row : pd.Series
            pandasの行データ
        """
        _type = type(row)
        assert _type == pd.Series
        self.title = row.title
        self.date = row.date_published
        self.draft = row.draft

        # share_img を整理する
        if row.share_img == "未分類":
            row.share_img = ""
        self.share_img = row.share_img

    def add_custom_values(self, row):
        """
        サブクラスで上書きするための関数。
        ここに書いておく必要はないのかもしれない

        Parameters
        ----------
        row : pd.Series
            pandasの行データ
        """
        _type = type(row)
        assert _type == pd.Series


@serde
class FMPost(FMBase):
    hosts: list[str] = field(default_factory=list)
    services: list[str] = field(default_factory=list)
    ages: list[str] = field(default_factory=list)
    subject: list[str] = field(default_factory=list)
    author: str = ""

    def add_custom_values(self, row):
        # タグを整理する（未分類は除外する）
        tags = set(
            [
                row.host_display,
                row.tag,
                row.service,
                row.age,
                row.language,
                row.news,
                row.upcoming,
                row.subject1,
                row.subject2,
            ]
        )
        tags.discard("未分類")

        # 教科を整理する（未分類は除外する）
        subject = set([row.subject1, row.subject2])
        subject.discard("未分類")

        # ふりがなを追加する
        if row.title_kana == "未分類":
            row.title_kana = ""

        self.subtitle = row.title_kana
        self.tags = sorted(tags)
        self.categories = [row.category]
        self.hosts = [row.host_display]
        self.services = [row.service]
        self.ages = [row.age]
        self.subject = sorted(subject)
        self.author = row.host_short


@serde
class FMReport(FMBase):
    pass


@serde
class FMPage(FMBase):
    pass


if __name__ == "__main__":
    """
    CSVファイルをきちんとロードできるかを確認するためのテスト関数を用意した。
    このファイルを実行するだけでテストができるようになっている。
    主にデータの入力値の型を確認していて、おかしな値があると失敗するようにしてある。

    $ python3 frontmatter.py

    TODO:
    - merger.pyで作成するCSVファイルの項目／内容を見直して、このモジュールから前処理をなくしたい
    - pytestを使ってかっこよく確認できるようにしたい
    """

    from pathlib import Path

    def test_fm_types(fm):
        """
        FrontMatterクラスの入力値の型を確認する

        クラス変数の型が想定している型と同じになっているかを
        assertを使って確認しているだけの簡単な内容になっている

        Parameters
        ----------
        fm : FMBase, FMPost, FMReport
            FrontMatter用の独自クラス
        """
        _name = type(fm)
        debug = f"Start: Test {_name}"
        logger.debug(debug)
        assert type(fm) in (FMBase, FMPost, FMReport)
        assert type(fm.title) == str
        assert type(fm.linkTitle) == str
        assert type(fm.subtitle) == str
        assert type(fm.date) == str
        assert type(fm.categories) == list
        assert type(fm.tags) == list
        assert type(fm.weight) == int
        assert type(fm.draft) == bool
        assert type(fm.share_img) == str

        if type(fm) == FMPost:
            assert type(fm.hosts) == list
            assert type(fm.services) == list
            assert type(fm.ages) == list
            assert type(fm.subject) == list
            assert type(fm.author) == str

        debug = f"End: Test {_name}"
        logger.debug(debug)

    def test_load_contents(data):
        """
        コンテンツ用のデータを読み込んで、入力値（の型）に間違いがないか確認する

        Parameters
        ----------
        data : pd.DataFrame
            pandasのデータフレーム
        """
        assert type(data) == pd.DataFrame

        for idx, row in data.iterrows():
            # 行データの前処理
            # NaNを未分類に置き換える
            row = row.fillna("未分類")
            fm = FMPost()
            fm.add_default_values(row)
            fm.add_custom_values(row)
            test_fm_types(fm)

    def test_load_reports(data):
        """
        レポート用のデータを読み込んで、入力値（の型）に間違いがないか確認する

        Parameters
        ----------
        data : pd.DataFrame
            pandasのデータフレーム
        """
        assert type(data) == pd.DataFrame

        for idx, row in data.iterrows():
            row = row.fillna("未分類")
            fm = FMReport()
            fm.add_default_values(row)
            fm.add_custom_values(row)
            test_fm_types(fm)

    fm = FMBase()
    test_fm_types(fm)

    fm = FMPost()
    test_fm_types(fm)

    fm = FMReport()
    test_fm_types(fm)

    fname = Path("contents.csv")
    data = pd.read_csv(fname)
    test_load_contents(data)

    fname = Path("reports.csv")
    data = pd.read_csv(fname)
    test_load_reports(data)
