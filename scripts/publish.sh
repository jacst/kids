#! /usr/bin/env bash

NOW=$(date '+%Y-%m-%d %H:%M:%S')
echo "==============================" >> last-updated
echo $NOW >> last-updated
echo "==============================" >> last-updated
git checkout release
git merge master
echo "-----[ Run parser ]--------------------" >> last-updated
poetry run ./builder.py --all
git status >> last-updated
MSG=$(echo "fix(auto-update): published content pages": $NOW)
git add ../content/
git commit -m "$MSG" >> last-updated
git push
git checkout master
