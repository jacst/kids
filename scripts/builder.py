#! /usr/bin/env python3

from pathlib import Path
from string import Template

import numpy as np
import pandas as pd
from frontmatter import FMPage, FMPost, FMReport
from loguru import logger


def add_content(row):
    """
    メインコンテンツを作成する

    Parameters
    ----------
    row : pd.Series_
        コンテンツの行データ

    Returns
    -------
    str
        コンテンツを並べた文字列
    """
    lines = ""
    lines += embed_video(row)
    lines += "\n\n<!--more-->\n\n"
    lines += add_pubdate(row)
    lines += add_url(row)
    lines += add_host(row)
    return lines


def add_pubdate(row):
    """Add published date"""
    lines = '## <i class="fas fa-calendar-check"></i> &nbsp; 公開日／掲載日'
    lines += "\n\n"
    lines += f"{row.date_published}\n\n"
    return lines


def add_host(row):
    """Add host (institution name)"""
    lines = "---\n\n"
    lines += '## <i class="fas fa-info-circle"></i> &nbsp; コンテンツの提供機関'
    lines += "\n\n"
    lines += f'- <i class="fas fa-landmark"></i> &nbsp; {row.host}'
    if row.host_short is not np.nan:
        lines += f"（{row.host_short}）"
    lines += "\n"
    lines += f'- <i class="fas fa-globe-asia"></i> &nbsp; {row.host_location}'
    lines += "\n"
    lines += f'- <i class="fas fa-laptop-code"></i> &nbsp; {row.host_url}'
    lines += "\n"
    lines += "\n\n"
    return lines


def add_url(row):
    """Add URL"""
    lines = '## <i class="fas fa-paperclip"></i> &nbsp; URL'
    lines += "\n\n"
    lines += f"- {row.url}\n"
    lines += "- 埋め込みが正しく表示されない場合は上記URLをクリックしてください\n\n"

    return lines


def embed_youtube(row):
    """Embed YouTube

    Hugoのショートコードを使ってYouTubeを埋め込む
    ショートコード用のタグは raw 文字列を使う
    中括弧を含むraw文字列とf文字列の組み合わせがうまくいかないようなので
    まずリスト型で作成し、空白文字を挟んで連結（join）することにした

    video_id を取得できなかった場合は、スキップする

    video_id の長さで単体 / 再生リストを判別する
    単体 = 11
    再生リスト = 34
    """

    if row.video_id is np.nan:
        return ""

    vid = row.video_id
    title = row.title_escaped
    if len(vid) < 15:
        code = [r"{{<", "youtube", f'id="{vid}"', f'title="{title}"', r">}}"]
    else:
        code = [r"{{<", "youtube-playlist", f'id="{vid}"', f'title="{title}"', r">}}"]
    lines = " ".join(code)
    lines += "\n\n"
    return lines


def embed_vimeo(row):
    """Embed Vimeo

    Hugoのショートコードを使ってVimeoを埋め込む
    """
    if row.video_id is np.nan:
        return ""

    vid = row.video_id
    title = row.title_escaped
    code = [r"{{<", "vimeo", f'id="{vid}"', f'title="{title}"', r">}}"]
    lines = " ".join(code)
    lines += "\n\n"
    return lines


def embed_niconico(row):
    """Embed NicoNico

    自作のショートコードを使ってをニコニコ動画を埋め込む
    """
    if row.video_id is np.nan:
        return ""

    vid = row.video_id
    title = row.title_escaped
    code = [r"{{<", "niconico", f'id="{vid}"', f'title="{title}"', r">}}"]
    lines = " ".join(code)
    lines += "\n\n"
    return lines


def embed_webpage(row):
    """Embed webpage

    自作のショートコードを使って動画以外のコンテンツを埋め込む

    1. ウェブサイトへのリンクを埋め込む
    2. YouTubeの再生リストを埋め込む
    """

    url = row.url
    title = row.title_escaped
    code = [r"{{<", "webpage", f'url="{url}"', f'title="{title}"', r">}}"]
    lines = " ".join(code)
    lines += "\n\n"
    return lines


def embed_video(row):
    """Embed video

    動画の埋め込み用のラインを生成する
    以下のようにフィルタリング
    1. video_id がない -> ""
    2. service名別に切り替え
    """
    if row.video_id is np.nan:
        return embed_webpage(row)
    if row.service == "YouTube":
        return embed_youtube(row)
    if row.service == "Vimeo":
        return embed_vimeo(row)
    if row.service == "ニコニコ":
        return embed_niconico(row)
    return ""


def add_footer(row):
    """Add footer

    ニュース利用についてコンテンツの末尾に追記する
    """

    if row.credit is np.nan:
        return ""

    lines = "---\n\n"
    lines += '## <i class="fas fa-copyright"></i> &nbsp; クレジット'
    lines += "\n\n"
    lines += f"{row.credit}\n"
    lines += "\n\n"

    if row.news is not np.nan:
        lines += "---\n\n"
        lines += '## <i class="fas fa-tv"></i> &nbsp; ニュースでの利用について'
        lines += "\n\n"

        if row.news == "ニュース利用可能":
            lines += "このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から承諾を得ています。\n"
            lines += "ご利用の際はクレジットの明記をお願いします。\n"
            lines += (
                "その他、ご不明な点に関しては、提供機関に直接お問い合わせください。\n"
            )
        elif row.news == "ニュース利用申請":
            lines += "このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から申請が必要だと聞いています。\n"
            lines += "提供機関に直接お問い合わせください。\n"
        else:
            lines += "このコンテンツをテレビなどのニュースの一部として利用することについて、提供機関の広報担当者から許諾を得ていません。\n"
            lines += "ご不明な点に関しては、提供機関に直接お問い合わせください。\n"

    lines += "\n\n"
    return lines


def add_debug(row):
    """Add content"""
    lines = "\n---\n"
    lines += "# for DEBUG\n\n"
    lines += f'- date_published = "{row.date_published}"\n'
    lines += f'- date_added = "{row.date_added}"\n'
    lines += f'- category = "{row.category}"\n'
    lines += f'- approved = "{row.approved}"\n'
    lines += f'- tag = "{row.tag}"\n'
    lines += f'- service = "{row.service}"\n'
    lines += f'- language = "{row.language}"\n'
    lines += f'- thumb = "{row.thumb}"\n'
    lines += f'- remarks = "{row.remarks}"\n'
    lines += f'- video_id = "{row.video_id}"\n'
    lines += f'- host_header = "{row.host_header}"\n'
    lines += f'- host = "{row.host}"\n'
    lines += f'- host_short = "{row.host_short}"\n'
    lines += f'- host_location = "{row.host_location}"\n'
    lines += f'- host_url = "{row.host_url}"\n'
    lines += f'- host_youtube_name = "{row.host_youtube_name}"\n'
    lines += f'- host_youtube_name = "{row.host_youtube_url}"\n'
    lines += "\n---\n"
    return lines


def build_posts(data, verbosity, debug):
    """
    メインコンテンツのmdファイルを生成する関数

    Parameters
    ----------
    data : pd.DataFrame
        pandasのデータフレーム
    verbosity : bool
        ログを表示する
    debug : bool
        mdファイルにデバッグ用の情報を書き込む
    """

    # 保存先のディレクトリを確認する
    saved = Path("../content/watch")
    if not saved.exists():
        raise FileNotFoundError
    # 使用するデータを抽出する
    is_approved = "approved == 'OK'"
    q = is_approved
    posts = data.query(q)

    for idx, row in posts.iterrows():
        savef = saved / f"v{idx+1:06d}.md"
        row = row.fillna("未分類")
        fm = FMPost()
        fm.add_default_values(row)
        fm.add_custom_values(row)
        lines = fm.as_toml()
        lines += add_content(row)
        lines += add_footer(row)

        if debug:
            lines += "---\n\n"
            lines += "# fm.as_debug()\n\n"
            lines += fm.as_debug()

        with open(savef, "w") as f:
            f.write(lines)
            if verbosity:
                debug = f"Wrote {savef}"
                logger.debug(debug)

    npages = len(posts)
    info = f"build_posts: Built {npages} pages"
    logger.info(info)
    return posts, npages


def build_featured(data, verbosity, debug):
    """
    おすすめコンテンツを10件抽出して、ページを作成する

    - 承認OK / 公開OK / 動画のみ
    - デバッグ時は同じコンテンツになるようにシードを固定する

    Parameters
    ----------
    data : pd.DataFrame
        コンテンツのデータ
    verbosity : bool
        ログを表示する
    debug : bool
        コンテンツをデバッグ
    """
    # 保存先のディレクトリを確認する
    saved = Path("../content/featured")
    if not saved.exists():
        raise FileNotFoundError

    # 使用するデータを抽出する
    is_approved = "approved == 'OK'"
    is_published = "draft == False"
    is_video = "tag == '動画'"
    q = (" and ").join([is_approved, is_published, is_video])
    qdata = data.query(q)

    if debug:
        featured = qdata.sample(10, random_state=0).reset_index()
    else:
        featured = qdata.sample(10).reset_index()

    for idx, row in featured.iterrows():
        savef = saved / f"v{idx+1:06d}.md"
        row = row.fillna("未分類")
        fm = FMPost()
        fm.add_default_values(row)
        fm.add_custom_values(row)
        lines = fm.as_toml()
        lines += add_content(row)
        lines += add_footer(row)

        if debug:
            lines += "---\n\n"
            lines += "# fm.as_debug()\n\n"
            lines += fm.as_debug()

        with open(savef, "w") as f:
            f.write(lines)
            if verbosity:
                debug = f"Wrote {savef}"
                ic(debug)

    npages = len(featured)
    info = f"build_featured: Built {npages} pages"
    ic(info)
    return featured, npages


def build_index(data, verbosity, debug, lastmod="2020-02-29"):
    """
    トップページ（コンテンツ数／機関数）のページを生成する関数

    Parameters
    ----------
    data : pd.DataFrame
        コンテンツのデータ
    verbosity : bool
        ログを表示する
    debug : bool
        デバッグモード
    """
    _INDEX = Template(
        """
## 公開コンテンツの掲載数

$npublished 件

## 協力機関の数

$nhosts の研究機関・大学など


## 最終更新日

$lastmod

---

> [2022-03-10 ウェブサイトをリニューアル！]({{< ref "post/migrated.md" >}})
> GoogleサイトからGitLab Pages（このサイト）に移行しました。

"""
    )

    # 保存先のディレクトリを確認する
    saved = Path("../content/")
    if not saved.exists():
        raise FileNotFoundError

    # 「掲載数」に使用するデータを抽出する
    is_approved = "approved == 'OK'"
    is_published = "draft == False"
    q = (" and ").join([is_approved, is_published])
    posts = data.query(q)
    npublished = len(posts)

    # 協力機関の数を計算する
    hosts = posts["host"].unique()
    nhosts = len(hosts)

    if verbosity:
        debug = f"掲載数 {npublished} / 機関数 {nhosts} / 更新日 {lastmod}"
        ic(debug)

    lines = _INDEX.substitute(npublished=npublished, nhosts=nhosts, lastmod=lastmod)

    savef = saved / "_index.md"
    with open(savef, "w") as f:
        f.write(lines)
        info = f"build_index: Updated {savef}"
        ic(info)

    return hosts, nhosts


def build_contributors(data, verbosity, debug, lastmod="2020-02-29"):
    """
    協力メンバーのページを生成する関数

    Parameters
    ----------
    data : pd.DataFrame
        協力メンバーのデータ
    verbosity : bool
        ログを表示する
    debug : bool
        デバッグモード
    """

    # 保存先のディレクトリを確認する
    saved = Path("../content/page")
    if not saved.exists():
        raise FileNotFoundError

    # 掲載OKなメンバーを抽出する
    is_confirmed = "emoji == '👍'"
    q = is_confirmed
    confirmed = data.query(q)

    nmembers = len(data)
    nconfirmed = len(confirmed)

    info = f"build_contributors: {nmembers} -> {nconfirmed}"
    ic(info)

    fm = FMPage()
    fm.title = "協力メンバー"
    fm.subtitle = "Contributors"
    fm.date = lastmod
    fm.weight = 100
    fm.bigimg = [{"src": "/kids/img/jacst_for_kids.jpg", "desc": f"{lastmod} 現在"}]
    fm.draft = False

    names = []
    for name in confirmed.name_display:
        names.append(f"- {name}")
    names = ("\n").join(names)

    body = fm.as_toml()
    body += "\n\n"
    body += names

    savef = saved / "contributors.md"
    with open(savef, "w") as f:
        f.write(body)
        info = f"build_contributors: Updated {savef}"
        ic(info)

    return


def build_reports(data, verbosity, debug):
    """
    新着情報のコンテンツを生成する関数

    - 公開OKなコンテンツを抽出する

    Parameters
    ----------
    data : pd.DataFrame
        新着情報／掲載情報のデータフレーム
    verbosity : bool
        ログを表示する
    debug : bool
        デバッグモード
    """

    # 保存先のディレクトリを確認する
    saved = Path("../content/post")
    if not saved.exists():
        raise FileNotFoundError

    # 掲載OKなデータを抽出する
    is_published = "draft == False"
    q = is_published
    posted = data.query(q)

    for idx, row in posted.iterrows():
        savef = saved / f"r{idx+1:06d}.md"
        fm = FMReport()
        fm.add_default_values(row)
        fm.add_custom_values(row)

        lines = fm.as_toml()

        with open(savef, "w") as f:
            f.write(lines)
            if verbosity:
                debug = f"Wrote {savef}"
                ic(debug)

    npages = len(posted)
    info = f"build_reports: Built {npages} pages"
    ic(info)


def get_lastmod(data):
    """
    コンテンツの最終更新日を取得する
    - 最終更新日は、公開済みのコンテンツを対象とする
    - 近日公開（upcoming）のコンテンツは対象から外している
    - （最終更新日がずっと未来になってしまう可能性があるため）

    Parameters
    ----------
    data : pd.DataFrame
        コンテンツの一覧

    Returns
    -------
    str
        最終更新日
    """

    # 対象となるデータを取り出す
    # approval == "OK" かつ upcoming == ""
    is_approved = "approved == 'OK'"
    is_past = "upcoming != 'upcoming'"
    q = (" and ").join([is_approved, is_past])
    qdata = data.query(q)
    lastmod = qdata.sort_values(by="date_published")["date_published"].dropna().iat[-1]
    return lastmod


if __name__ == "__main__":
    import argparse

    # オプションを設定
    parser = argparse.ArgumentParser(
        description="臨時休校対応特別サイトのコンテンツページを生成するスクリプト"
    )
    parser.add_argument("-i", "--index", action="store_true", help="build top page")
    parser.add_argument(
        "-f", "--featured", action="store_true", help="build featured pages"
    )
    parser.add_argument(
        "-c", "--contributors", action="store_true", help="build contributors page"
    )
    parser.add_argument(
        "-r", "--reports", action="store_true", help="build report pages"
    )
    parser.add_argument("-a", "--all", action="store_true", help="build all pages")
    parser.add_argument(
        "--no-posts",
        action="store_true",
        help="skip building watch pages. useful when debugging.",
    )
    parser.add_argument("--debug", action="store_true", help="set debug = True")
    parser.add_argument("--verbose", action="store_true", help="set verbose = True")
    args = parser.parse_args()

    if args.all:
        args.index = True
        args.featured = True
        args.contributors = True
        args.reports = True

    # print(args)

    fname = Path("contents.csv")
    data = pd.read_csv(fname)

    # タイトルに含まれるクォーテーションをエスケープする
    # double quote をエスケープする
    # single quote はそのままでOK
    data["title_escaped"] = data["title"].str.replace('"', '\\"')

    # 掲載OKなコンテンツに限定する
    qdata = data.query('approved == "OK"')

    # 最終更新日を取得する
    lastmod = get_lastmod(qdata)

    if not args.no_posts:
        print("Building post pages")
        build_posts(qdata, args.verbose, args.debug)

    if args.featured:
        print("Building featured pages")
        build_featured(qdata, args.verbose, args.debug)

    if args.index:
        print("Building tope page")
        build_index(qdata, args.verbose, args.debug, lastmod=lastmod)

    if args.contributors:
        print("Building contributors page")
        fname = Path("snapd") / "_jk_contributors.csv"
        data = pd.read_csv(fname, skiprows=1)
        build_contributors(data, args.verbose, args.debug, lastmod=lastmod)

    if args.reports:
        print("Building report pages")
        fname = Path("reports.csv")
        data = pd.read_csv(fname)
        build_reports(data, args.verbose, args.debug)
