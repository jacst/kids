#! /usr/bin/env python3

from pathlib import Path
from urllib.parse import urlparse
from loguru import logger
import pandas as pd


def get_youtube_id(url):
    """
    URLからYouTubeの動画IDを取得する。
    通常URLと短縮URLのどちらからも動画IDを取り出すことができる。
    URLがYouTube動画でないと判断した場合は None を返す。

    Examples:
    - https://youtu.be/_u48I195FzI = 短縮URL
    - https://www.youtube.com/watch?v=uR9cDGNyd4A = 通常のURL
    """
    o = urlparse(url)
    if o.netloc == "youtu.be":
        return o.path[1:]
    elif o.netloc in ("www.youtube.com", "youtube.com"):
        if o.path == "/watch":
            _index = o.query.index("v=")
            return o.query[_index + 2 : _index + 13]
        elif o.path[:7] == "/embed/":
            return o.path.split("/")[2]
        elif o.path[:3] == "/v/":
            return o.path.split("/")[2]
        elif o.path == "/playlist":
            _index = o.query.index("list=")
            return o.query[_index + 5 : _index + 39]
    return None


def get_youtube_playlist_id(url):
    """
    URLからYouTubeの再生リストIDを取得する。
    URLがYouTube再生リストでないと判断した場合は None を返す。

    Examples
    - https://www.youtube.com/playlist?list=PLAb15H07mjvjJL_d5UcZqDWhF1Yc_CVOm = 再生リスト
    """
    o = urlparse(url)
    if o.netloc in ("www.youtube.com", "youtube.com"):
        if o.path == "/playlist":
            _index = o.query.index("list=")
            return o.query[_index + 5 : _index + 39]
    return None


def get_vimeo_id(url):
    """
    URLからVimeo動画のIDを取得する。
    URLがVimeo動画でないと判断した場合は None を返す。

    Examples:
    - https://vimeo.com/206421215
    - https://vimeo.com/395892549
    """
    o = urlparse(url)
    if o.netloc == "vimeo.com":
        return o.path[1:]
    return None


def get_niconico_id(url):
    """
    URLからニコニコ動画／生放送の動画IDを取得する。
    URLがニコニコでないと判断した場合は None を返す。

    Examples:
    - https://live2.nicovideo.jp/watch/lv326410782
    - https://live2.nicovideo.jp/watch/lv318957159
    - https://live.nicovideo.jp/watch/lv313142329
    """
    o = urlparse(url)
    if o.netloc in ("live.nicovideo.jp", "live2.nicovideo.jp"):
        return o.path.split("/")[-1]
    return None


def get_video_id(url):
    """URLから動画のIDを取得する

    次の順番で動画IDを取得する
    1. YouTubeのIDを取得する
    2. VimeoのIDを取得する
    3. ニコニコ動画のIDを取得する
    """
    vid = get_youtube_id(url)
    if vid is None:
        vid = get_youtube_playlist_id(url)
    if vid is None:
        vid = get_vimeo_id(url)
    if vid is None:
        vid = get_niconico_id(url)
    return vid


def add_share_img(data):
    """YouTube動画にサムネイルを取得する

    YouTube動画のサムネイルはURLの雛形が決まっており、
    動画IDが分かればURLを生成することができる。
    生成したURLは share_img のカラムに追加する。
    """

    def share_img(vid):
        """YouTube動画のサムネイルURLを生成する。

        動画IDがない場合は "" を返す。
        YouTube再生リスト（=PLから始まる動画ID）の場合は "" を返す。
        YouTube動画の場合はURLを生成する。

        サムネイルの解像度に応じてURLが用意されている。

        - 1920x1080 : http://img.youtube.com/vi/動画ID/maxresdefault.jpg
        -  640x 480 : http://img.youtube.com/vi/動画ID/sqdefault.jpg
        -  480x 360 : http://img.youtube.com/vi/動画ID/hqdefault.jpg
        -  320x 180 : http://img.youtube.com/vi/動画ID/mqdefault.jpg
        -  120x  90 : http://img.youtube.com/vi/動画ID/default.jpg

        TODO:
        サムネイルにアクセスできない動画もあるっぽい
        """
        if not vid:
            return ""
        if vid.startswith("PL"):
            return ""
        url = f"https://img.youtube.com/vi/{vid}/hqdefault.jpg"
        return url

    data["share_img"] = ""

    # YouTubeの動画のコンテンツを抽出する
    isT = data.service == "YouTube"
    data.loc[isT, "share_img"] = data["video_id"].map(share_img)

    return data


def check_draft(data):
    """掲載OKなコンテンツか確認する

    データフレームに下書き用のカラム（draft）追加する。
    掲載OKな場合は draft = False にする。
    掲載可否の承認結果はスプレッドシートで管理している。
    """

    data["draft"] = True
    isT = data.approved == "OK"
    data.loc[isT, "draft"] = False
    return data


def check_upcoming(data):
    """近日公開のコンテンツかをどうかを確認する。

    データフレームに近日公開用カラム（upcoming）を追加する。
    公開日が未来の場合は upcoming = "近日公開" にする。
    当日のコンテンツも「近日公開」に含めるため、基準は「ビルド日時の前日」にする。

    ビルド日時 = 2022-04-27 14:44:35.229228
    1日前      = 2022-04-26 14:44:35.229228

    4月26日公開のコンテンツ <-- x
    4月27日公開のコンテンツ <-- 近日公開
    4月28日公開のコンテンツ <-- 近日公開
    """

    # 現在日時を取得する
    # 基準は「昨日の日付」にする
    yesterday = pd.Timestamp.today() - pd.Timedelta(days=1)
    logger.info(yesterday)

    # カラムを追加する；デフォルトは ""
    data["upcoming"] = ""

    # 現在日時と公開日を比較する
    isT = data["date_published"] > yesterday
    data.loc[isT, "upcoming"] = "upcoming"
    return data


if __name__ == "__main__":
    datad = Path("snapd")

    # コンテンツのリストを作成する
    fname1 = datad / "_jk_contents.csv"
    fname2 = datad / "_jk_hosts.csv"

    # コンテンツ一覧を pd.DataFrame として読み込む
    data1 = pd.read_csv(fname1, skiprows=2, parse_dates=["date_published"])

    # 不要なカラムを削除する
    data1.drop(columns=["thumb", "iframe"], inplace=True)

    # 掲載OKな場合は draft = False にする
    data1 = check_draft(data1)
    # URLから動画のIDを取り出す
    data1["video_id"] = data1["url"].map(get_video_id)

    # YouTubeの動画のみOGP画像を追加する
    data1 = add_share_img(data1)

    # 現在日時と公開日を比較する
    data1 = check_upcoming(data1)

    # 参加機関の一覧を pd.DataFrame として読み込む
    data2 = pd.read_csv(fname2, skiprows=2)

    # コンテンツ一覧と参加機関の一覧をマージする
    merged = pd.merge(data1, data2, on="host", how="left", indicator=True)
    info = f'Merged dataframes ("{fname1}" / "{fname2}")'
    logger.info(info)

    fname = "contents.csv"
    merged.to_csv(fname, index=False)
    info = f"Saved as {fname}"
    logger.info(info)

    # レポートのリストを作成する
    fname3 = datad / "_jk_reports.csv"
    data3 = pd.read_csv(fname3, skiprows=1)

    # 不要なカラムを削除する
    data3.drop(
        columns=["date_applied", "company", "name_applied", "name_responded"],
        inplace=True,
    )

    # 掲載OKな場合は draft = False にする
    data3 = check_draft(data3)

    fname = "reports.csv"
    data3.to_csv(fname, index=False)
    info = f"Saved as {fname}"
    logger.info(info)
