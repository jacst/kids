#! /usr/bin/env bash

set -e

function usage {
cat <<EOM
    Usage:
    $(basename "$0") [OPTION]...

Options:
    -h  show this help
    -l  show log (TBA)
    -x  commit & push to remote repository

Description:
    JACSTの臨時休校対応特別サイトの自動更新
    1. スプレッドシートの取得（更新がなくても）
    2. マスターデータの作成
    3. ウェブページの生成
    の一連の作業を定期的に実行する

    定期実行の方法
    1. 定期実行の際はLaunchdに登録して使う
    2. ../launchd/に移動する
    3. make install / reinstall /uninstall する
    実行内容は Makfile を確認すれば分かる

EOM
    exit 2
}

# コンテンツの更新
function run {
    NOW=$(date '+%Y-%m-%d %H:%M:%S')
    echo "==============================" > last-updated
    echo $NOW >> last-updated
    echo "==============================" >> last-updated
    echo "-----[ Run snapsheets ]--------------------"
    poetry run snapsheets
    echo "-----[ Run merger ]--------------------"
    poetry run ./merger.py
    echo "-----[ Run parser ]--------------------"
    poetry run ./builder.py
    echo "-----[ Finished ] --------------------"
    git status
}

# 更新があった場合にリポジトリにコミット
# 必ずmasterブランチで作業することにした
function runx {
    git checkout master
    run
    echo "1) ------------------------------" >> last-updated
    git status >> last-updated
    MSG=$(echo "fix(auto-update): auto-update content pages": $NOW)
#    echo $MSG 2>&1 | tee -a last-updated
    git add snapd/
    git add contents.csv
    git add reports.csv
    git add ../content/
    echo "2) ------------------------------" >> last-updated
    git commit -m "$MSG" >> last-updated
}

while getopts ":hlx" OPTIONS
do
    case "$OPTIONS" in
        h) OPT_FLAG_HELP=1;;
        l) OPT_FLAG_LOG=1;;
        x) OPT_FLAG_X=1;;
        :) echo "[ERROR] Undefined options."; OPT_FLAG_HELP=1;;
        \?) echo "[ERROR] Undefined options."; OPT_FLAG_HELP=1;;
    esac
done


if [ -n "${OPT_FLAG_HELP}" ];then
    usage;
elif [ -n "${OPT_FLAG_X}" ]; then
    runx;
else
    run;
fi
