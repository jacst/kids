# 確認事項

- [ ] 同じような内容の機能追加リクエストがないことを確認しました

# どのような機能でしょうか？（What is this feature ?）

（どのような機能なのか教えてください）

# どうして必要なのでしょうか？（Why do you need the feature ?）

（どうしてこの機能が必要なのか教えてください）

# どのように実装すればいいでしょうか？（How do you implement the feature ?）

（もし実装する際のアイデアをお持ちであれば教えてください）
