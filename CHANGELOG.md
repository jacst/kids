## 1.2.1 (2024-09-28)

### Fix

- **config/_default/config.toml**: メニューを変更した

## 1.2.0 (2024-09-28)

### Feat

- **config/blowfish/hugo.toml**: Blowfish用の設定を追加した

### Fix

- **content/watch/v000007.md**: URLを更新した
- **scripts/frontmatter.py**: icecreamをloguruで置き換えた
- **pyproject.toml**: icecreamを削除した: 2.1.3
- **scripts/merger.py**: 元データのカラム名変更に合わせて修正した
- **scripts/config.toml**: 設定ファイルをデフォルト名に変更した
- **scripts/snapd/**: コンテンツ一覧を更新した
- **scripts/sheets.toml**: v1系の形式に変更した
- **pyproject.toml**: pyserdeを追加した: 0.20.1
- **pyproject.toml**: icecreamを追加した: 2.1.3
- **pyproject.toml**: loguruを追加した: 0.7.2
- **pyproject.toml**: pytestを追加した: 8.3.3
- **pyproject.toml**: ruffを追加した: 0.6.8
- **pyproject.toml**: snapsheetsを更新した: 0.6.1 -> 1.0.0
- **pyproject.toml**: Python 3.10以上にした
- **pyproject.toml**: pandasを更新した: 1.4.2 -> 2.2.3
- **pyproject.toml**: 依存パッケージをリセットした
- **pyproject.toml**: pysenの設定を削除した
- **pyproject.toml**: devパッケージを削除した
- **.pre-commit-config.yaml**: hookを追加した: ruff: 0.6.7
- **.pre-commit-config.yaml**: hookを追加した
- **.pre-commit-config.yaml**: hookを追加した: pre-commit-hooks
- **.pre-commit-config.yaml**: hookを更新: commitizen: 2.20.5 -> 3.29.1
- **blowfish/content/posts**: 新聞掲載などのトピックスを追加した
- **blowfish/content/about/_index.md**: orderByWeightを追加した
- **blowfish/config/blowfish/params.toml**: 記事リストの設定を追加した
- **blowfish/config/blowfish/params.toml**: ナビゲーションの表示方法を変更した
- **blowfish/content/_index.md**: トップページを追加した
- **blowfish/config/blowfish/params.toml**: カラースキームを変更した
- **blowfish/content/posts/**: トピックスを追加した
- **blowfish/config/blowfish/params.toml**: 記事の設定を修正した
- **.gitignore**: resourcesを除外した
- **blowfish/config/blowfish/params.toml**: デフォルト画像を追加した
- **blowfish/content/about**: aboutを追加した
- **blowfish/config/blowfish/params.toml**: backgroundにした
- **blowfish/assets/img/jacst_for_kids.jpg**: ロゴを追加した
- **blowfish/config/blowfish/**: テーマの設定を移動した renamed:    config/blowfish/hugo.toml -> blowfish/config/blowfish/hugo.toml renamed:    config/blowfish/languages.ja.toml -> blowfish/config/blowfish/languages.ja.toml renamed:    config/blowfish/markup.toml -> blowfish/config/blowfish/markup.toml renamed:    config/blowfish/menus.ja.toml -> blowfish/config/blowfish/menus.ja.toml renamed:    config/blowfish/params.toml -> blowfish/config/blowfish/params.toml
- **.gitmodules**: 以前追加したテーマを削除した
- **.gitmodules**: テーマを更新した
- **.gitmodules**: 再度追加した
- **.gitmodules**: 一旦削除した
- **.gitmodules**: themes/blowfishを戻した
- **blowfish/themes/blowfish**: ディレクトリ名を変更した
- **hugo/archetypes/default.md**: 追加した
- **hugo/config/_default/hugo.toml**: 設定ファイルを追加した
- **hugo/themes/blowfish**: Blowfishを追加した: main
- **themes/blowfish**: Blowfishを更新した: v2.77.1
- **config/blowfish/markup.toml**: markupのパラメータを設定中
- **config/blowfish/menus.ja.toml**: メニューのパラメータを設定中
- **config/blowfish/hugo.toml**: グローバルなパラメータを設定中
- **config/blowfish/params.toml**: テーマのパラメータを設定中
- **config/blowfish/languages.ja.toml**: 日本語の設定ファイルを追加した

## 1.1.0 (2023-11-13)

### Feat

- **themes/jacst-theme**: 専用テーマを作成しはじめた

### Fix

- **auto-update**: auto-update content pages: 2022-10-03 14:30:30
- **auto-update**: auto-update content pages: 2022-10-03 14:30:04
- **launchd/local.jacstkids.plist**: fix working directory path
- **auto-update**: auto-update content pages: 2022-06-22 14:30:05
- **auto-update**: auto-update content pages: 2022-06-22 10:30:05
- **scripts/builder.py:add_footer**: ニュース利用の文章を修正した
- **scripts/builder.py**: クレジットは別セクションにした
- **scripts/frontmatter.py**: タグの機関名を host -> host_dislpay に変更した
- **auto-update**: auto-update content pages: 2022-06-22 09:30:05
- **auto-update**: auto-update content pages: 2022-06-22 00:30:06
- **auto-update**: auto-update content pages: 2022-06-21 15:42:20
- **scripts/builder.py**: オプションを整理した
- **sandbox/sheets.toml**: 作業用のディレクトリを作成した
- **auto-update**: auto-update content pages: 2022-05-20 14:30:05
- **content/post/**: changed tags to categories

## 1.0.0 (2022-05-17)

### Fix

- **workflow.drawio.svg**: ウェブサイト更新のワークフロー図を追加した
- **auto-update**: auto-update content pages: 2022-04-28 16:30:01
- **scripts/builder.py**: draft == Falseの条件式を再び修正した
- **auto-update**: auto-update content pages: 2022-04-27 20:30:05
- **auto-update**: auto-update content pages: 2022-04-27 20:07:57
- **auto-update**: auto-update content pages: 2022-04-27 20:03:35
- **auto-update**: auto-update content pages: 2022-04-27 16:30:05
- **auto-update**: auto-update content pages: 2022-04-27 15:04:07
- **scripts/merger.py**: 近日公開の判定の日時を「前日」に変更した
- **scripts/merger.py**: 近日公開の判定の日時を「前日」に変更した
- **scripts/builder.py**: build_index / build_reports のフィルタも修正した
- **scripts/builder.py**: build_featuredのデータフィルタの値を修正した
- **i18n/ja.yaml**: seeAlsoの日本語訳を「関連コンテンツ」に変更した
- **auto-update**: auto-update content pages: 2022-04-27 09:27:14
- **config/_default/config.toml**: sitemapの設定を追加した
- **config/_default/config.toml**: sitemapの設定を追加した
- **auto-update**: auto-update content pages: 2022-04-26 17:30:05
- **auto-update**: auto-update content pages: 2022-04-26 16:30:05
- **auto-update**: auto-update content pages: 2022-04-26 16:07:54
- **auto-update**: auto-update content pages: 2022-04-26 13:15:19
- **snapd/_jk_contents.csv**: 動画タイトルのふりがなを有効にした
- **content/watch/**: コンテンツのページを更新した
- **scripts/builder.py**: オプションの説明を表示することにした
- **scripts/builder.py**: オプション名を変更した
- **scripts/__init__.py**: __version__を修正した
- **docs**: ドキュメント用のディレクトリを追加した
- **tests**: pytest用のディレクトリを追加
- **scripts/__init__.py**: バージョン（__version__）を追加した

### Feat

- **delete-files**: parser.pyとreporter.pyを削除した
- **scripts/publish.sh**: parser.pyをbuilder.pyに置き換えた
- **scripts/run.sh**: parser.pyをbuilder.pyに置き換えた
- **scripts/run.sh**: parser.pyをbuilder.pyに置き換えた

## 0.9.1 (2022-04-25)

### Fix

- **auto-update**: auto-update content pages: 2022-04-25 17:30:04
- **auto-update**: auto-update content pages: 2022-04-25 09:53:39
- **auto-update**: auto-update content pages: 2022-04-22 00:30:05
- **auto-update**: auto-update content pages: 2022-04-14 19:30:03

## 0.9.0 (2022-04-14)

### Fix

- **themes/beautifulhugo**: テーマ（submodule）を更新した
- **scripts/builder.py**: 実行権限を追加した
- **scripts/builder.py**: lastmodを所得する位置を再検討した
- **scripts/frontmatter.py**: bigimgのクラス変数を追加した
- **scripts/builder.py**: オプションを整理した
- **auto-update**: auto-update content pages: 2022-04-14 00:30:03
- **scripts/reporter.py**: deprecatedをimportした
- **scripts/parser.py**: buildAllオプションを追加した
- **scripts/parser.py**: deprecatedのデコレータを追加した
- **scripts/builder.py**: flake8でチェックしたエラー箇所を修正した
- **scripts/builder:build_index**: デバッグ情報を修正した
- **scripts/builder:build_index**: トップページを書き出すようにした
- **scripts/builder:build_featured**: おすすめ10コンテンツのファイルを書き出せるようにした
- **scripts/builder:add_content**: parser.pyからまるっとコピペした
- **scripts/builder.py**: icの表示内容を修正した
- **scripts/builder:build_index**: トップページを書き出す機能を追加した
- **scripts/builder:build_contributors**: ファイルに書き出す部分を追加した
- **scripts/frontmatter:FMBase**: クラス変数にsubtitleを追加した
- **scripts/builder.py**: buildAllオプションを追加した

### Feat

- **scripts/frontmatter.py**: フロントマターでふりがな（subtitle）を設定できるようにした
- **scripts/builder:build_reports**: 新着情報を作成する関数を追加した
- **scripts/builder:build_contributors**: 協力メンバーのページを生成する関数を追加した
- **scripts/frontmatter:FMPage**: ページ用のクラスを追加した
- **scripts/builder:build_index**: トップページを生成するための関数を追加した
- **scripts/builder:build_featured**: おすすめ10コンテンツをビルドする関数を追加した
- **scripts/builder:build_posts**: メインコンテンツをビルドするための関数を追加した
- **scripts/builder.py**: ビルド用のスクリプトを追加した（枠だけ作った状態）
- **scripts/frontmatter.py**: ファイル名を間違えていたので修正した
- **scripts/fromtmatter.py**: FrontMatterを扱うクラスファイルを追加した

### Refactor

- **pysen:lint**: flake8でチェックしたエラー箇所を修正した

## 0.8.0 (2022-04-13)

### Fix

- **auto-update**: auto-update content pages: 2022-04-07 13:30:00
- **auto-update**: auto-update content pages: 2022-04-06 15:30:00
- **auto-update**: auto-update content pages: 2022-04-01 00:30:04
- **i18n/ja.yaml**: readMoreの日本語を修正した
- **i18n/ja.yaml**: i18nのファイルをテーマからコピーした
- **auto-update**: auto-update content pages: 2022-03-31 15:30:05
- **auto-update**: auto-update content pages: 2022-03-29 17:30:05
- **auto-update**: auto-update content pages: 2022-03-25 00:30:05
- **content/_index.md**: インデックスを更新した
- **content/post/migrated.md**: 移行したお知らせの内容を修正した
- **scripts/parser.py**: _index.mdのテンプレートを修正した
- **migrated.md**: ファイル名を変更した
- **auto-update**: auto-update content pages: 2022-03-24 00:30:05
- **content/post/renewed.md**: リニューアル記事の内容を修正した
- **content/_index.md**: トップページの内容を修正した
- **config/_default/config.toml**: メニュー名を変更した
- **content/post**: 掲載情報を再ビルドした
- **snapsheets**: スプレッドシートを更新した
- **content/post/renewed.md**: リニューアルのお知らせを追加した
- **content/post/welcome.md**: ファイルをリネームした
- **content/post**: ブログ記事を一旦全部削除した
- **content**: コンテンツを更新した
- **scripts/parser.py**: 正しく埋め込まれてない場合の注意書きを修正した
- **scripts/parser.py**: ニュース利用に関するテンプレート文を修正した
- **content/watch/v000351.md**: アイコンを追加できるか試した
- **config/_default/config.toml**: authorのiconをlandmarkに設定した
- **themes/beautifulhugo**: テーマを更新した

### Feat

- **config/_default/config.toml**: goldmarkの設定を変更した
- **scripts/parser.py**: コンテンツにアイコンを追加した
- **config/_default/config.toml**: goldmarkの設定を変更した

## 0.7.0 (2022-03-23)

### Fix

- **content/page/**: 固定ページのタイトルとサブタイトルを入れ替えた
- **spreadsheet**: コンテンツを更新した
- **merger.py**: share_imgのURLのprintを削除した
- **content**: コンテンツを更新した
- **spreadsheet**: スプレッドシートを更新した
- **auto-update**: auto-update content pages: 2022-03-23 10:30:04
- **content**: コンテンツを追加した
- **spreadsheet**: スプレッドシートを更新した
- **auto-update**: auto-update content pages: 2022-03-18 00:30:05
- **content**: コンテンツを更新した
- **content**: コンテンツを更新した
- **content**: コンテンツを更新した

### Feat

- **merger.add_share_img**: YouTube動画のみサムネイル画像を設定できるようにした
- **merger.add_share_img**: YouTube動画のみサムネイル画像を設定できるようにした
- **parser.FrontMatter**: フロントマターに share_img を追加した
- **merger.py**: share_img のカラムを追加した

## 0.6.0 (2022-03-10)

### Fix

- **publish.sh**: 新しく作ったオプションを追加した

### Feat

- **parser**: オプション --buildContributors を追加した
- **parser**: オプション --buildIndex を追加した

## 0.5.0 (2022-03-10)

### Fix

- **content**: コンテンツを更新した
- **subject/_index.md**: 教科・科目から探すのインデックスを作成した
- **parser.make_frontmatter**: タグ一覧に未分類と教科追加した
- **config.toml**: GitLabアカウントを追加した

### Feat

- **parser.FrontMatter**: フロントマターに subject を追加した
- **config.toml**: メニューとタクソノミーに subject を追加した

## 0.4.0 (2022-03-07)

### Fix

- **content**: コンテンツを更新した
- **merger.py**: 近日公開 -> upcoming に変更した
- **parser.py**: tagにupcomingを追加した
- **auto-update**: auto-update content pages: 2022-03-04 19:30:04
- **themes**: テーマを更新した
- **auto-update**: auto-update content pages: 2022-02-28 21:30:05
- **auto-update**: auto-update content pages: 2022-02-28 14:30:04
- **publish.sh**: リリース用スクリプトのログメッセージを修正した
- **local.jacstkids.plist**: 定期実行ファイルを修正した
- **run.sh**: ログメッセージを修正した
- **run.sh**: スクリプトの説明を修正した
- **published.sh**: コミットメッセージを変更した
- **auto-update**: auto-update content pages: 2022-02-25 22:30:05
- **content/watch**: draft = true のファイルは削除した

### Feat

- **parser:get_lastmod**: 最終更新日の取得方法を再考した
- **config.toml**: メニューに近日公開を追加した
- **merger.py**: upcoming のカラムを追加した
- **config.toml**: buildFutureを有効にした
- **run.sh**: コンテンツを更新する部分と、リモートリポジトリにプッシュする部分を分けた

### Refactor

- **parser.py**: 使っていないコードを削除した

## 0.3.0 (2022-02-25)

### Feat

- **parser.py**: 公開済みのコンテンツから最後の公開日を取得するようにした
- おすすめを生成するオプションを追加した
- おすすめを生成するオプションを追加した
- **spreadsheet**: リニューアル記事の文章を修正した
- **config.toml**: デフォルトのOGP画像を設定した
- **themes**: beautifulhugoを更新した
- **spreadsheet**: 教科のカラムを追加した

### Fix

- **parser.py**: 掲載OKのコンテンツに限定した
- **auto-update**: auto-update content pages: 2022-02-25 17:30:03
- **auto-update**: auto-update content pages: 2022-02-25 10:30:05
- **auto-update**: auto-update content pages: 2022-02-25 09:30:02
- **auto-update**: auto-update content pages: 2022-02-25 08:30:05
- **auto-update**: auto-update content pages: 2022-02-25 07:30:01
- **auto-update**: auto-update content pages: 2022-02-25 06:30:04
- **auto-update**: auto-update content pages: 2022-02-25 05:30:05
- **parser.py**: トップページのテンプレート（デバッグ用）を編集した
- **auto-update**: auto-update content pages: 2022-02-25 04:30:03
- **auto-update**: auto-update content pages: 2022-02-25 03:30:03
- **auto-update**: auto-update content pages: 2022-02-25 02:30:05
- **auto-update**: auto-update content pages: 2022-02-25 01:30:03
- **auto-update**: auto-update content pages: 2022-02-25 00:42:52
- **auto-update**: auto-update content pages: 2022-02-25 00:30:04
- **auto-update**: auto-update content pages: 2022-02-24 23:30:05
- **auto-update**: auto-update content pages: 2022-02-24 22:30:00
- **languages/_index.md**: うまく表示できないことを正直に書いた
- **reports.csv**: リニューアルしてます記事を少し修正した
- **auto-update**: auto-update content pages: 2022-02-24 21:30:05
- **auto-update**: auto-update content pages: 2022-02-24 20:30:05
- **auto-update**: auto-update content pages: 2022-02-24 19:30:04
- **auto-update**: auto-update content pages: 2022-02-24 18:30:04
- **auto-update**: auto-update content pages: 2022-02-24 17:30:05
- **auto-update**: auto-update content pages: 2022-02-24 16:30:04
- **auto-update**: auto-update content pages: 2022-02-24 15:30:04
- **auto-update**: auto-update content pages: 2022-02-24 14:30:03
- **auto-update**: auto-update content pages: 2022-02-24 13:30:05
- **auto-update**: auto-update content pages: 2022-02-24 12:30:02
- **auto-update**: auto-update content pages: 2022-02-24 11:30:03
- **auto-update**: auto-update content pages: 2022-02-24 10:30:05
- **auto-update**: auto-update content pages: 2022-02-24 09:30:02
- **auto-update**: auto-update content pages: 2022-02-24 08:30:02
- **auto-update**: auto-update content pages: 2022-02-24 07:30:04
- **auto-update**: auto-update content pages: 2022-02-24 06:30:05
- **auto-update**: auto-update content pages: 2022-02-24 05:30:00
- **auto-update**: auto-update content pages: 2022-02-24 04:30:05
- **auto-update**: auto-update content pages: 2022-02-24 03:30:02
- **auto-update**: auto-update content pages: 2022-02-24 02:30:04
- **auto-update**: auto-update content pages: 2022-02-24 01:30:05
- **auto-update**: auto-update content pages: 2022-02-24 00:30:00
- **auto-update**: auto-update content pages: 2022-02-23 23:30:02
- **auto-update**: auto-update content pages: 2022-02-23 22:30:04
- **auto-update**: auto-update content pages: 2022-02-23 21:30:04
- **auto-update**: auto-update content pages: 2022-02-23 20:30:05
- **auto-update**: auto-update content pages: 2022-02-23 20:24:23

## 0.2.0 (2022-02-23)

### Fix

- **parser.py**: host -> host_display
- **auto-update**: auto-update content pages: 2022-02-23 19:48:14
- **auto-update**: auto-update content pages: 2022-02-23 19:30:05
- **auto-update**: auto-update content pages: 2022-02-23 19:22:27
- **auto-update**: auto-update content pages: 2022-02-23 18:30:04
- **auto-update**: auto-update content pages: 2022-02-23 17:30:05
- **auto-update**: auto-update content pages: 2022-02-23 16:30:03
- **auto-update**: auto-update content pages: 2022-02-23 15:30:05
- **auto-update**: auto-update content pages: 2022-02-23 14:30:05
- **auto-update**: auto-update content pages: 2022-02-23 13:30:03
- **auto-update**: auto-update content pages: 2022-02-23 12:29:42
- **auto-update**: auto-update content pages: 2022-02-22 20:09:23
- **auto-update**: auto-update content pages: 2022-02-22 20:02:39
- **auto-update**: auto-update content pages: 2022-02-22 19:56:28
- **auto-update**: auto-update content pages: 2022-02-22 19:51:48
- **auto-update**: auto-update content pages: 2022-02-22 19:42:57
- **auto-update**: auto-update content pages: 2022-02-22 18:30:05
- **auto-update**: auto-update content pages: 2022-02-22 17:30:05
- **auto-update**: auto-update content pages: 2022-02-22 16:30:06
- **auto-update**: auto-update content pages: 2022-02-22 15:30:02
- **auto-update**: auto-update content pages: 2022-02-22 14:30:05
- **auto-update**: auto-update content pages: 2022-02-22 14:20:59
- **auto-update**: auto-update content pages: 2022-02-22 14:20:45
- **run.sh**: git pushするのをやめた

### Feat

- タグに表示する機関名を短くした
- タグに表示する機関名を短くした

## 0.1.2 (2022-02-22)

### Fix

- **puslish.sh**: releaseブランチにpushする手順を整理した
- **auto-update**: auto-update content pages: 2022-02-22 14:11:14
- **auto-update**: auto-update content pages: 2022-02-22 14:07:48
- **run.sh**: リダイレクトを修正した
- **auto-update**: auto-update content pages: 2022-02-22 14:06:29
- **run.sh**: 実行手順を修正した
- **auto-update**: auto-update content pages: 2022-02-22 14:05:12
- **auto-update**: auto-update content pages: 2022-02-22 14:02:35
- **auto-update**: auto-update content pages: 2022-02-22 13:30:05
- **auto-update**: auto-update content pages: 2022-02-22 12:30:04
- **auto-update**: auto-update content pages: 2022-02-22 11:30:05
- **auto-update**: auto-update content pages: 2022-02-22 10:30:03
- **auto-update**: auto-update content pages: 2022-02-22 09:30:05
- **auto-update**: auto-update content pages: 2022-02-22 08:30:04
- **auto-update**: auto-update content pages: 2022-02-22 07:30:04
- **auto-update**: auto-update content pages: 2022-02-22 06:30:05
- **auto-update**: auto-update content pages: 2022-02-22 05:30:04
- **auto-update**: auto-update content pages: 2022-02-22 04:30:05
- **auto-update**: auto-update content pages: 2022-02-22 03:30:04
- **auto-update**: auto-update content pages: 2022-02-22 02:30:05
- **auto-update**: auto-update content pages: 2022-02-22 01:30:00
- **auto-update**: auto-update content pages: 2022-02-22 00:30:04
- **auto-update**: auto-update content pages: 2022-02-21 23:30:03
- **auto-update**: auto-update content pages: 2022-02-21 23:04:38
- **auto-update**: auto-update content pages: 2022-02-21 23:03:27
- **parser.py**: おすすめを生成するときの debugモードをOFFにした
- **parser.py:add_pubdate**: 公開日／掲載日に変更した
- **parser.py**: add_pubdateを修正した

## 0.1.1 (2022-02-21)

### Fix

- **config.toml**: 関連サイトを追加した
- **config.toml**: 関連サイトを追加した
- **run.sh**: コメントを外した
- **run.sh**: メッセージログの取り方を修正した
- **run.sh**: チェックアウトするブランチをmasterに変更した
- **auto-update**: auto-update content pages: 2022-02-19 23:29:15
- **auto-update**: auto-update content pages: 2022-02-19 23:29:15
- **parser.py**: トップページのテンプレートを修正した
- **parser.py**: トップページのテンプレートを修正した
- **auto-update**: auto-update content pages: 2022-02-18 22:30:05
- **auto-update**: auto-update content pages: 2022-02-18 22:30:05
- **auto-update**: auto-update content pages: 2022-02-18 21:35:37
- **auto-update**: auto-update content pages: 2022-02-18 21:33:58
- **auto-update**: auto-update content pages: 2022-02-18 21:30:05
- **auto-update**: auto-update content pages: 2022-02-18 20:37:20

## 0.1.0 (2022-02-18)

### Fix

- **_index.md**: トップページにサイト移行について説明を追加した
- **auto-update**: auto-update content pages: 2022-02-18 17:55:57
- **auto-update**: auto-update content pages: 2022-02-14 21:07:47
- **parser.py**: lastmodをきちんと取得するようにした
- **img**: faviconの元データも追加した
- **favicons**: faviconに関係したファイルを追加した

### Feat

- **config.toml**: faviconを追加した

## 0.0.2 (2022-02-14)

### Fix

- **auto-update**: auto-update content pages: 2022-02-14 14:30:00
- **themes**: テーマをサブモジュールに置き換えた
- **themes**: 既存のテーマを削除した
- **auto-update**: auto-update content pages: 2022-02-14 13:31:56
- **issue-template**: 機能追加のテンプレートを修正した
- **issue-template**: バグ報告のテンプレートを微修正
- **issue-template**: バグ報告のテンプレートを修正した
- **CONTRIBUTING.md**: 編集の方針を修正した
- **CONTRIBUTING.md**: 貢献する方法について書いてみた
- **issue-template**: 機能追加用のテンプレートを追加した
- **issue-template**: バグ報告用のテンプレートを追加した
- **run.sh**: 更新した
- **auto-update**: auto-update content pages: 2022-02-10 19:32:21
- **auto-update**: auto-update content pages: 2022-02-10 19:31:38
- **opend.md**: 開設したお知らせのファイル名を変更した
- **git rm**: 不要なコンテンツを削除した
- **spreadsheet**: コンテンツを追加した
- **config.toml**: メニュー名を変更した
- **content/post/r*.md**: コンテンツを追加した
- **reporter.py**: レポート作成スクリプトを作成した
- **merger.py**: draft を判別する関数を追加した
- **spreadsheet**: シートの内容を更新した
- **merger.py**: 生成するファイル名を変更した
- **auto-update**: auto-update content pages: 2022-02-09 21:53:51
- **sheets.toml**: 活動報告のシートURLを追加した
- **parser.py**: fixed misc
- **parser.py**: ウェブサイトが埋め込めなくなってしまったのを修正した
- **merger.py**: 動画のIDを取得することにした
- **merger.py**: 不要な操作を削除した
- **merger.py**: いまは不要になった操作をコメントアウトした
- **merger.py**: parserの仕事をmergerに移行した
- **merger.py**: 掲載OKの確認する場所を変更した
- **merger.py**: 不要なカラムを削除した
- **parser.py**: タイトルのエスケープ処理する場所を工夫した
- **parser.py**: 不要な関数を削除した
- **parser.py**: 不要な関数をコメントアウトした
- **parser.py**: make_frontmatterにリネームした
- **parser.py**: コンテンツ生成部分を整理した
- **parser.py**: frontmatterの作り方を変更した
- **parser.py**: しばらくデバッグモードにしておく
- **parser.py**: ランダム表示のデバッグをオプションに変更した
- **parser.py**: ランダムに並べ替えた順に表示できるようにした
- **parser.py**: weight を設定できるようにした
- **parser.py**: featuredコンテンツの作成時に公開OKなものだけにした
- **parser.py**: draftを追加した
- **merger.py**: draftのカラムを追加した
- **parser.py**: featuredに変更したので修正した
- **parser.py**: メニューにおすすめを追加した
- **parser.py**: featured にパス名を変更した
- **parser.py**: エラー処理を追加した
- **parser.py**: デバッグ用に乱数シードを固定した
- **content/recommend/v*.md**: コンテンツを追加した
- **parser.py**: recommended pages を生成できるようにした
- **parser.py**: frontmatter用のdataclassをpyserdeを使って作成した
- **shortcodes/*.html**: added BS4のCSSクラス
- **niconico.html**: 非推奨の属性を削除した
- **youtube-playlist.html**: 非推奨な属性を削除した
- **content/watch/v*.md**: 追加し忘れた更新
- **content/watch/v*.md**: 再生リストの埋め込み
- **youtube-playlist.html**: divタグを追加した
- **spreadsheet**: 理研CBSのコンテンツのURLを修正した
- **spreadshet**: 理研CBSのコンテンツを修正した
- **spreadsheet**: 理研BDRのYouTubeのURLを変更した
- **spreadsheet**: OISTスタイムのURLをvimeoに修正した
- **parser.py**: get_youtube_playlist_id に分割した
- **youtube-playlist.html**: ショートコードを追加した
- **parser.py**: 動画 / 再生リストの区別ができた
- **parser.py**: 再生リストのIDを取得
- **auto-update**: auto-update content pages: 2022-02-06 03:30:04
- **spreadsheet**: カラム名を変更した
- **parser.py**: 置換し忘れていた箇所を修正した
- **parser.py**: add_fm_date も修正した
- **parser.py**: カラム名を変更する分岐を追加した
- **head.html**:  titleを修正した
- **head.html**:  titleを修正した
- **auto-update**: auto-update content pages: 2022-02-04 19:30:05
- **auto-update**: auto-update content pages: 2022-02-04 12:30:02
- **run.sh**: 更新用のブランチをcheckoutすることにした
- **auto-update**: auto-update content pages: 2022-02-03 14:30:06
- **auto-update**: auto-update content pages: 2022-02-03 12:30:05
- **auto-update**: auto-update content pages: 2022-02-02 17:30:04
- **auto-update**: auto-update content pages: 2022-02-02 16:54:17
- **run.sh**: git push を追加した
- **auto-update**: auto-update content pages: 2022-02-02 16:30:02
- **parser.py**: languages は使えなさそうなので一旦コメントアウト
- **parser.py**: frontmatter に ages を追加した
- **parser.py**: frontmatter に services を追加した
- **parser.py**: lang -> language に置き換えた
- **parser.py**: ages -> age に置き換えた
- **parser.py**: platform -> service に置き換えた
- **spreadsheet**: カラム名を変更した
- **config.toml**: メニューを修正した
- **config.toml**: 探す系のメニューを細分化した
- **hosts/_index.md**: 機関別一覧のタイトルページを作成した
- **auto-update**: auto-update content pages: 2022-02-02 13:30:04
- **auto-update**: auto-update content pages: 2022-02-02 13:30:04
- **.gitignore**: ignore scripts/last-updated
- **run.sh**: last-updated をコミットするのをやめた
- **auto-update**: auto-update content pages: 2022-02-02 11:30:05
- **auto-update**: auto-update content pages: 2022-02-02 10:30:02
- **auto-update**: auto-update content pages: 2022-02-02 09:30:03
- **auto-update**: auto-update content pages: 2022-02-02 08:30:00
- **auto-update**: auto-update content pages: 2022-02-02 07:30:04
- **auto-update**: auto-update content pages: 2022-02-02 06:30:04
- **auto-update**: auto-update content pages: 2022-02-02 05:30:01
- **auto-update**: auto-update content pages: 2022-02-02 04:30:03
- **auto-update**: auto-update content pages: 2022-02-02 03:30:04
- **auto-update**: auto-update content pages: 2022-02-02 02:30:05
- **auto-update**: auto-update content pages: 2022-02-02 01:30:02
- **auto-update**: auto-update content pages: 2022-02-02 00:30:05
- **auto-update**: auto-update content pages: 2022-02-01 23:30:05
- **auto-update**: auto-update content pages: 2022-02-01 22:30:05
- **auto-update**: auto-update content pages: 2022-02-01 21:30:04
- **auto-update**: auto-update content pages: 2022-02-01 20:30:04
- **auto-update**: auto-update content pages: 2022-02-01 19:30:04
- **auto-update**: auto-update content pages: 2022-02-01 18:58:00
- **auto-update**: auto-update content pages: 2022-02-01 18:48:44
- **auto-update**: auto-update content pages: 2022-02-01 18:38:36
- **auto-update**: auto-update content pages: 2022-02-01 18:30:04
- **auto-update**: auto-update content pages: 2022-02-01 17:30:04
- **auto-update**: auto-update content pages: 2022-02-01 16:30:03
- **auto-update**: auto-update content pages: 2022-02-01 15:30:05
- **auto-update**: auto-update content pages: 2022-02-01 15:23:09
- **auto-update**: auto-update content pages: 2022-02-01 14:30:05
- **auto-update**: auto-update content pages: 2022-02-01 13:30:02
- **auto-update**: auto-update content pages: 2022-02-01 12:30:05
- **auto-update**: auto-update content pages: 2022-02-01 11:30:05
- **auto-update**: auto-update content pages: 2022-02-01 10:30:04
- **auto-update**: auto-update content pages: 2022-02-01 09:30:05
- **auto-update**: auto-update content pages: 2022-02-01 08:30:04
- **auto-update**: auto-update content pages: 2022-02-01 07:30:04
- **auto-update**: auto-update content pages: 2022-02-01 06:30:05
- **auto-update**: auto-update content pages: 2022-02-01 05:30:04
- **auto-update**: auto-update content pages: 2022-02-01 04:30:05
- **auto-update**: auto-update content pages: 2022-02-01 03:30:05
- **auto-update**: auto-update content pages: 2022-02-01 02:30:05
- **auto-update**: auto-update content pages: 2022-02-01 01:30:04
- **auto-update**: auto-update content pages: 2022-02-01 00:30:04
- **auto-update**: auto-update content pages: 2022-01-31 23:30:05
- **auto-update**: auto-update content pages: 2022-01-31 22:30:05
- **auto-update**: auto-update content pages: 2022-01-31 21:30:05
- **auto-update**: auto-update content pages: 2022-01-31 21:19:44
- **content/page/*.md**: コンテンツを分割した
- **parser.py**: contributors.mdを更新できるようにした
- **contributors.md**: weightを追加した
- **page/*.md**: 内容を分割した
- **auto-update**: auto-update content pages: 2022-01-31 20:45:44
- **content/watch/v*.md**: コンテンツを更新した
- **spreadsheet**: コンテンツを修正した
- **auto-update**: auto-update content pages: 2022-01-31 20:30:04
- **auto-update**: auto-update content pages: 2022-01-31 19:30:05
- **auto-update**: auto-update content pages: 2022-01-31 18:30:04
- **auto-update**: auto-update content pages: 2022-01-31 17:30:05
- **auto-update**: auto-update content pages: 2022-01-31 16:30:05
- **auto-update**: auto-update content pages: 2022-01-31 16:18:57
- **auto-update**: auto-update content pages: 2022-01-31 15:30:04
- **auto-update**: auto-update content pages: 2022-01-31 14:30:04
- **auto-update**: auto-update content pages: 2022-01-31 13:41:35
- **config.toml**: フッターの表示名・日付を修正した
- **config.toml**: フッターの表示名・日付を修正した
- **auto-update**: auto-update content pages: 2022-01-31 12:30:04
- **auto-update**: auto-update content pages: 2022-01-31 11:30:03
- **auto-update**: auto-update content pages: 2022-01-31 10:30:05
- **auto-update**: auto-update content pages: 2022-01-31 09:47:45
- **auto-update**: auto-update content pages: 2022-01-31 09:47:45
- **auto-update**: auto-update content pages: 2022-01-31 09:30:04
- **auto-update**: auto-update content pages: 2022-01-31 09:01:03
- **auto-update**: auto-update content pages: 2022-01-30 04:30:04
- **auto-update**: auto-update content pages: 2022-01-30 03:30:05
- **auto-update**: auto-update content pages: 2022-01-30 02:30:03
- **auto-update**: auto-update content pages: 2022-01-30 01:30:05
- **auto-update**: auto-update content pages: 2022-01-30 00:30:05
- **auto-update**: auto-update content pages: 2022-01-29 23:30:05
- **auto-update**: auto-update content pages: 2022-01-29 22:57:17
- **auto-update**: auto-update content pages: 2022-01-29 22:52:26
- **run.sh**: 定期実行時のメッセージを変更した
- **local.jacstkids.plist**: 定期実行を毎時30分に変更した
- **regular-update**: auto-update content pages: 2022-01-29 22:48:12
- **run.sh**: 自動実行スクリプトを丁寧に書いた
- **regular-update**: auto-update content pages: 2022-01-29 22:46:24
- **Makefile**: タブ幅を修正した
- **Makefile**: launchdの操作用コマンドをまとめた
- **local.jacstkids.plist**: 作成する場所を間違えていた
- **local.jacstkids.plit**: plistを作成した
- **run.sh**: 一連の作業をシェルスクリプトにまとめた
- **parser.py**: 余分な print を削除した
- **content/watch/v*.md**: ほぼすべてのコンテンツが埋め込めるようになった
- **parser.py**: embed_webpageを追加した
- **webpage.html**: ショートコードに引数を追加した
- **webpage.html**: ウェブページを埋め込むショートコードを作成した
- **config.toml**: aboutのmenuを修正した
- **about.md**: date を追加した
- **parser.py**: オプションの使い方を変更した
- **_index.md**: コンテンツの最終更新日が分かるようにした
- **parser.py**: コンテンツの最終更新日を表示することにした
- **parser.py**: bigimg を追加できるようにした
- **contributor.md**: page の下に移動した
- **snapsheet**: emoji も使えることがわかった
- **contributors.md**: まずテキストで作成した
- **sheets.toml**: 協力者の一覧を取得できるようにした
- **content/watch/v*.md**: コンテンツを更新した
- **snapsheet**: プラットフォームの選択間違いを修正した
- **parser.py**: ニコニコ動画が埋め込めたことを確認した
- **parser.py**: ニコニコ動画のIDが取得できていることを確認した
- **parser.py**: 不要な print を削除した
- **content/watch/v*.md**: 比較用のファイルを用意した
- **parser.py**: ニコニコ動画のIDを取得できるようになった
- **niconico.html**: ニコニコ用のショートコードを作成した
- **parser.py**: embed_video に embed_vimeo を追加した
- **parser.py**: embed_vimeo を作成した
- **parser.py**: get_video_id で vimeoのIDも取得できるようにした
- **spreadsheet**: プラットフォームのタグが間違っていた項目を修正した
- **parser.py**: embed_video を作成した
- **parser.py**: vimeoの動画IDのパーサーを作成した
- **parser.py**: get_video_idをget_youtube_id に変更した
- **post_preview.html**: 続きを読むボタンのスタイルを修正した
- **post_meta.html**: カテゴリタグのFAアイコンを変更した
- **config.toml**: readingTime / wordCount を無効にした
- **parser.py**: _index.md にコンテンツの掲載数を表示した
- **parser.py**: _index.mdのテンプレートを少し修正した
- **parser.py**: _index.mdのテンプレートを修正した
- **parser.py**: _index.md用のテンプレートを追加した
- **parser.py**: frontmatter に author を追加した
- **parser.py**: frontmatter用の関数に prefix をつけることにした
- **content/watch/v*.md**: コンテンツを更新した
- **parser.py**: オプションを追加（debugとverbose）
- **parser.py**: 区切り線を追加した
- **merged.csv**: 機関情報を更新した
- **parser.py**: 提供機関の関連情報を追加した
- **parser.py**: 埋め込みが表示されないときのメッセージの表示位置を変更した
- **parser.py**: コンテンツの表示順を変更した
- **content/watch/v*.md**: コンテンツを更新した
- **parser.py**: 機関名に略称も追加した
- **parser.py**: デバッグ表示を追加した
- **merger.py**: printを追加した
- **merger.py**: printを修正した
- **content/watch/v*.md**: コンテンツを更新した
- **merged.csv**: マージしたCSVを追加した
- **merger.py**: コンテンツ + ホストをマージするスクリプトを追加した
- **_jk_hosts.csv**: カラム名を変更した
- **_jk_hosts.csv**: カラム名を変更した
- **merger.py**: とりあえず追加
- **snapsheet**: 参加機関のデータを追加した
- **rename**: _jk_contents.csvに変更した
- **sheets.toml**: 参加機関の一覧を追加した
- **sheets.toml**: コンテンツのDL名を変更した
- コンテンツを追加し忘れていた
- **content/watch/v*.md**: コンテンツを更新した
- **spreadsheet**: 元データを修正した
- **content/watch/v*.md**: コンテンツを更新した
- **scripts/parser.py**: tags の要素をソートすることにした
- **spreadsheet**: 元データを更新した
- **scripts/parser.py**: tagsにいろいろ追加した
- **snapsheet**: 元データを更新した
- **content/watch/v*.md**: コンテンツを更新した
- **spreadsheet**: 機関名の表記を整理した
- **content/watch/v*.md**: コンテンツを更新した
- **config.toml**: メニューの表示を修正した
- **content/watch/v*.md**: コンテンツを更新した
- **scripts/parser.py**: カラム名を変更 type -> tag
- **scripts/parser.py**: カラム名を変更 toc -> category
- **spreadsheet**: カラム名を変更した
- **config.toml**: menu をまとめた
- **content/watch/v*.md**: コンテンツを更新した
- **scripts/parser.py**: タグの内容を修正した
- **scripts/parser.py**: カテゴリに目次を追加した
- **config.toml**: menuにhostsを追加した
- **content/watch/v*.md**: コンテンツを更新した
- **scripts/parser.py**: frontmatterにhostsを追加した
- **scripts/parser.py**: カラム名を name -> host に変更した
- **spreadsheet**: カラム名を name -> host に変更した
- **config.toml**: taxonomies に host を追加した
- **_index.md**: トップページを修正した
- **config.toml**: トップのbigimgのパスも修正した
- **config.toml**: メニューを修正した
- **about.md**: bigimg を表示できるようにした
- サンプルコードをdraft = true にした
- bigimgのパスに気をつければいいことがわかった
- page と post に bigimg を追加した
- page と post に bigimg を追加した
- **content/watch/v*.md**: コンテンツを更新した
- **scripts/parser.py**: YouTubeの動画を埋め込めるようにした
- **scripts/parser.py**: get_video_id を作成した
- **i18n/ja.yaml**: 日本語がおかしいので英語をそのまま使うことにした
- **i18n/ja.yaml**: pageNotFoundのメッセージを修正した
- **i18n/ja.yaml**: dateFormatを修正した
- **i18n/ja.yaml**: 言葉 -> 単語に変更した
- **themes/bf/i18n/ja.yaml**: 投稿 -> 掲載に変更した
- **config.toml**: logo を削除した
- **config.toml**: bigimgの書き方を修正した
- **config.toml**: titleを修正sita
- **config.toml**: トップページに画像を追加した
- **content/watch/v*.md**: コンテンツファイルを更新した
- **spreadsheet**: 日付の表示形式を変更した
- **config.toml**: テーマ更新に合わせて設定ファイルを確認した
- **config.toml**: テーマ更新に合わせて設定ファイルを確認した
- **themes**: beautifulhugo を更新した
- **thems**: 一時的にリネームした
- **config.toml**: GAを追加した
- **config.toml**: 管理者名を変更した
- **content/page/about.md**: サイトポリシーを追記した
- **posts**: 画像へのパスが間違っていたので修正した
- **posts**: サンプルをdraftに変更した
- **config.toml**: メニューを修正した
- **config.toml**: トップページの画像のキャプションを変更した
- **content/post/2020-02-29**: ウェブサイト開設のお知らせを掲載した
- draft = true にしても表示される
- **content/page/about.md**: Aboutの内容を書き換えた
- **content/watch/v*.md**: コンテンツを更新した
- **scripts/parser.py**: ニュース利用に関する文言を修正した
- **scripts/parser.py**: メインコンテンツの書き出し部分を分割して改良した
- **scripts/parser.py**: 動画が埋め込まれない場合は、URLを表示することにした
- **scripts/parser.py**: 動画のタイトルを変えて、YouTubeを埋め込んだ
- **scripts/parser.py**: とりあえず全ページに同じ動画を表示した
- **content/watch/v*.md**: コンテンツを更新した
- **scripts/parser.py**: タグから重複とnanを除外した
- **scripts/parser.py**: import numpy
- **README.md**: コンテンツ生成スクリプトの使い方を追記した
- **sheets.toml**: シートの説明（desc）を追加した
- **snapd/*.csv**: シートをCSVで追加した
- **content/watch/v*.md**: コンテンツを更新した
- **scripts/parser.py**: 古い関数を削除した
- **conf.py**: デバッグ用の表示内容を修正した
- **scripts/parser.py**: メインのコンテンツを書き出す関数を作成した
- **scripts/parser.py**: 関数名を変更した
- **scripts/parser.py**: frontmatterにdraftを追加した
- **scripts/parser.py**: frontmatterにdateを追加した
- **scripts/parser.py**: 掲載許可がNGの場合はdraftモードにする
- **scripts/parser.py**: フィルタを削除した
- delete unused files
- **scripts/parser.py**: フィルタを追加した
- **scripts/parser.py**: コンテンツを書き出す関数を追加した
- **scripts/parser.py**: single quote はそのままでOK
- **scripts/parser.py**: タイトル中のdouble quote をエスケープした
- **scripts/parser.py**: テンプレート文字列を囲む文字を double quote に変更した
- **content/watch/v000*.md**: un-escape double quote
- **scripts/parser.py**: escape double quotes
- **content/watch/v0000*.md**: title with single quote
- **parser.py**: fixed import order
- **content/wach/v*.md**: ページを追加した
- **scripts/parser.py**: テンプレートを微修正した
- **scripts/parser.py**: メインのスクリプトを作成した
- **config.toml**: タイポを修正した
- **sheets.toml**: 休校中サイトのシート情報を追加した
- **config.toml**: categories をメニューに追加した
- **content/watch/v*.md**: コンテンツを追加
- **config.toml**: 管理者情報を整理した
- **config.toml**: タイトルを再度変更した
- **config.toml**: タイトルを変更した
- **config.toml**: disabled comments
- **config.toml**: enabled bigimg
- **config.toml**: 日本語に変更した
- **config.toml**: baseurlを設定した
- **config.toml**: moved to config/_default/config.toml
- **.gitignore**: ignore macOS related files
- **.gitignore**: ignore hugo related files
